﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    public int score = 0;
    private Text txtScore;

	// Use this for initialization
	void Start () {
        txtScore = GameObject.Find("Text").GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {

        txtScore.text = "Wynik: " + score;
	}
}
