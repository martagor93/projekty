﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GhostMove : MonoBehaviour {

    public Transform[] waypoints;
    int cur = 0;
    private Game game;
    private Text gameOver;
    public AudioClip eatenPacman;
    private AudioSource source;

    public float speed = 0.3f;
    private float time;
    private float finishScareTime;
    private float StartScareTime;

    private bool calmGhosts = false;
    public PacmanMove pacman;

    public void Start()
    {
        game = GameObject.Find("Game").GetComponent<Game>();
        gameOver = GameObject.Find("GameOver").GetComponent<Text>();
        time = game.howLongScared * 0.32f * 0.22f;
        gameOver.enabled = false;
        source = GetComponent<AudioSource>();
        source.enabled = true;
    }
    void FixedUpdate()
    {
        initializeAnimation();
    }
    void initializeAnimation()
    {
        if (transform.position != waypoints[cur].position)
        {
            Vector2 p = Vector2.MoveTowards(transform.position,
                                            waypoints[cur].position,
                                            speed);
            GetComponent<Rigidbody2D>().MovePosition(p);
        }
        // Waypoint reached, select next one
        else cur = (cur + 1) % waypoints.Length;

        // Animation
        Vector2 dir = waypoints[cur].position - transform.position;
        GetComponent<Animator>().SetFloat("X", dir.x);
        GetComponent<Animator>().SetFloat("Y", dir.y);
    }
    void OnTriggerEnter2D(Collider2D co)
    {
        if (co.name == "pacman")
        {
            if ((Game.livesNumber > 0) && (Game.livesNumber <=3))
            {
                FinishScaringGhosts();
                source.PlayOneShot(eatenPacman, 1f);
                game.SubtractLife();
                if (Game.livesNumber == 0)
                {
                    gameOver.enabled = true;
                    Time.timeScale = 0;
                }
            }
        }
    }

    public void Scare()
    {
        StartScareTime = Time.time + game.howLongScared * 0.62f;
        finishScareTime = StartScareTime;
        GetComponent<Animator>().SetBool("FinishBeingScared", false);
        StartScaringGhosts();
    }
    public void StartScaringGhosts()
    {
        GetComponent<Animator>().SetBool("StartBeingScared", true);
        if (Time.time >= finishScareTime && Time.time >= StartScareTime)
            changeGhostColor();
    }
    public void FinishScaringGhosts()
    {
        finishScareTime = 0;
        StartScareTime = 0;
        GetComponent<Animator>().SetBool("FinishBeingScared", false);
        GetComponent<Animator>().SetBool("StartBeingScared", false);
    }
    public void changeGhostColor()
    {
        GetComponent<Animator>().SetBool("FinishBeingScared", true);
        finishScareTime = Time.time + time;
    }
}
