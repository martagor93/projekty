﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game : MonoBehaviour {

    public static int livesNumber = 3;
    public static int score;
    public enum GameState {game_started,game_over};
    public static GameState gamestate;

    private static bool scaredGhosts;
    public float howLongScared;
    private float calmGhosts;

    private GameObject pacman;
    private GameObject blinky;
    private GameObject pinky;
    private GameObject inky;
    private GameObject clyde;

    public static Game _instance;
    public static Game instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<Game>();
                DontDestroyOnLoad(_instance.gameObject);
            }
            return _instance;
        }
    }

	//Use this for initialization
	void Start () {
        gamestate = GameState.game_started;
        clyde = GameObject.Find("Clyde");
        pinky = GameObject.Find("pinky");
        inky = GameObject.Find("Inky");
        blinky = GameObject.Find("blinky");
        pacman = GameObject.Find("pacman");
    }
	
	// Update is called once per frame
	void Update () {

        if (scaredGhosts && calmGhosts <= Time.time)
        {
            blinky.GetComponent<GhostMove>().FinishScaringGhosts();
            pinky.GetComponent<GhostMove>().FinishScaringGhosts();
            inky.GetComponent<GhostMove>().FinishScaringGhosts();
            clyde.GetComponent<GhostMove>().FinishScaringGhosts();
        }
    }
    public void ResetScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        gamestate = GameState.game_started;
    }
    public void ScareGhosts()
    {
        clyde = GameObject.Find("Clyde");
        pinky = GameObject.Find("pinky");
        inky = GameObject.Find("Inky");
        blinky = GameObject.Find("blinky");
        scaredGhosts = true;
        blinky.GetComponent<GhostMove>().Scare();
        inky.GetComponent<GhostMove>().Scare();
        clyde.GetComponent<GhostMove>().Scare();
        pinky.GetComponent<GhostMove>().Scare();
        calmGhosts = Time.time + howLongScared;
    }
    public void SubtractLife()
    {
        livesNumber--;
        gamestate = GameState.game_over;
        LifeCountImage ui = GameObject.FindObjectOfType<LifeCountImage>();
        Destroy(ui.lives[ui.lives.Count - 1]);
        ui.lives.RemoveAt(ui.lives.Count - 1);
    }
    public static void DestroySelf()
    {
        score = 0;
        livesNumber = 3;
        Destroy(GameObject.Find("Game"));
    }
}
