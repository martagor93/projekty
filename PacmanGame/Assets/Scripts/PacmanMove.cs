﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PacmanMove : MonoBehaviour {
    public float velocity=0.4f;
    Vector2 target = Vector2.zero;
    Game game;
    bool dead = false;

    [Serializable]
    public class PointSprites
    {
        public GameObject[] pointSprites;
    }

    public PointSprites points;

    public static int killstreak = 0;

    // Use this for initialization
    public void Start () {
        target = transform.position;
        game = GameObject.Find("Game").GetComponent<Game>();
    }
    IEnumerator PlayDeadAnimation()
    {
        yield return new WaitForSeconds(2);
        dead = false;
        game.ResetScene();
    }
    // Update is called once per frame
    void FixedUpdate()
    {

        switch (Game.gamestate)
        {
            case Game.GameState.game_started:
                Vector2 movement = Vector2.MoveTowards(transform.position, target, velocity);
                GetComponent<Rigidbody2D>().MovePosition(movement);

                if ((Vector2)transform.position == target)
                {
                    if (Input.GetKey(KeyCode.UpArrow) && valid(Vector2.up))
                        target = (Vector2)transform.position + Vector2.up;
                    if (Input.GetKey(KeyCode.RightArrow) && valid(Vector2.right))
                        target = (Vector2)transform.position + Vector2.right;
                    if (Input.GetKey(KeyCode.DownArrow) && valid(-Vector2.up))
                        target = (Vector2)transform.position - Vector2.up;
                    if (Input.GetKey(KeyCode.LeftArrow) && valid(-Vector2.right))
                        target = (Vector2)transform.position - Vector2.right;
                }
                Vector2 dir = target - (Vector2)transform.position;
                GetComponent<Animator>().SetFloat("X", dir.x);
                GetComponent<Animator>().SetFloat("Y", dir.y);
                break;

            case Game.GameState.game_over:
                if (!dead)
                    StartCoroutine("PlayDeadAnimation");
                break;
        }
    }
    bool valid(Vector2 direction)
    {
        Vector2 position = transform.position;
        RaycastHit2D hit = Physics2D.Linecast(position + direction, position);
        return (hit.collider == GetComponent<Collider2D>());
    }
    public void GetPointsForEatenGhosts()
    {
        killstreak++;

        // limit killstreak at 4
        if (killstreak > 4) killstreak = 4;

        Instantiate(points.pointSprites[killstreak - 1], transform.position, Quaternion.identity);
        GameObject.Find("Canvas").GetComponent<Score>().score += (int)Mathf.Pow(2, killstreak) * 100;
        Game.score += (int)Mathf.Pow(2, killstreak) * 100;
    }
}
