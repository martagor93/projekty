﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PacmanScares : MonoBehaviour
{

    private Game game;
    public AudioClip eatenDot;
    private AudioSource source;
    // Use this for initialization
    void Start()
    {
        game = GameObject.Find("Game").GetComponent<Game>();
        source = GetComponent<AudioSource>();
        source.enabled = true;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.name == "pacman")
        {
            source.PlayOneShot(eatenDot, 0.8f);
            game.ScareGhosts();
            Destroy(gameObject);
        }
    }
}
