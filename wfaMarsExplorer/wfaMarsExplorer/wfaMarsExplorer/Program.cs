﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Diagnostics;


namespace wfaMarsExplorer {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {

            sym.Init();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new fMain());
        }
    }

    public
    class point: ICloneable{
         
        public static readonly IReadOnlyList<point> mozliweruchy = new List<point>() {
            new point(1,0),
            new point(1,1),
            new point(0,1),

            new point(-1,0),
            new point(-1,-1),
            new point(0,-1),

            new point(-1,1),
            new point(1,-1)
        };

        public int x;
        public int y;
        public point(int X, int Y) { this.x = X; this.y = Y; }
        public static point operator +(point p1, point p2) {
            return new point(p1.x + p2.x, p1.y + p2.y);
        }
        public static point operator -(point p1, point p2) {
            return new point(p1.x - p2.x, p1.y - p2.y);
        }
        public static point losuj(robotState r) {
            return new point(
            r.random.Next(-1, 2),
            r.random.Next(-1, 2)
            );
        }

        public static point krokWzdluzSciezki(point pozycja) {
            point point = new point(0, 0);

            blockBase baza = sym.mapa.baza;

            Dictionary<point, double> rozwiazanie = new Dictionary<point, double>();

            foreach (point p in point.mozliweruchy) {
                if (sym.mapa[pozycja + p] is blockRadioactive ||
                    sym.mapa[pozycja + p] is blockResource
                    )
                    rozwiazanie.Add((point)p.MemberwiseClone(), h.obliczOdlegloscEuklidesowa(baza.pozycja, pozycja + p));
            }

            var r2 = rozwiazanie.OrderByDescending(_ => _.Value).ToList();

            if (r2.Count == 0)
                r2.Add(new KeyValuePair<point, double>(new point(0, 0),0));

            return r2[0].Key;
        }

        public static point krokWStroneBazy(point pozycja) {
            point point = new point(0, 0);

            blockBase baza = sym.mapa.baza;

            Dictionary<point, double> rozwiazanie = new Dictionary<point, double>();

            foreach(point p in point.mozliweruchy) {
                rozwiazanie.Add((point)p.MemberwiseClone(), h.obliczOdlegloscEuklidesowa(baza.pozycja, pozycja + p));
            }

            var r2 = rozwiazanie.OrderBy(_ => _.Value).ToList();

            return r2[0].Key;
        }
        public override string ToString() {
            return string.Format("x: {0}, y: {1}", x, y);
        }

        public object Clone() {
            return new point(this.x, this.y);
        }
    }

    public
    class size {
        public int width;
        public int height;
        public size(int width, int height) { this.width = width; this.height = height; }
        public static size operator +(size p1, int zmiana) {
            return new size(p1.width + zmiana, p1.height + zmiana);
        }
        public static size operator -(size p1, int zmiana) {
            return new size(p1.width - zmiana, p1.height - zmiana);
        }
    }
    public
    static class h {
        // zamienic na helper'a
        private static readonly object randLock = new object();
        static
        public
        double obliczOdlegloscEuklidesowa(point a, point b) {
            return Math.Sqrt(Math.Pow(a.x - b.x, 2) + Math.Pow(a.y - b.y, 2));
        }

        public static class random {
            private static Random rand = new Random(Environment.TickCount);
            public static int Next(int v1, int v2) {
                lock (randLock) {
                    return rand.Next(v1, v2);
                }
            }
        }
    }

    static
    class sym {
        public static readonly object key = new object();
        
        public static int liczbaRobotow = 50;
        public static int predkosc = 20;

        public static mapData mapa;
        public static robot[] robot;
        public static chainBehaviour chain = new chainBehaviour();
        public static void Init() {

            mapa = mapData.Generuj(new size(50, 50));

            robot = new robot[liczbaRobotow];

            for(int i = 0; i < liczbaRobotow; i++) {
                robot[i] = new robot(i);
                robot[i].task.Start();
            }
        }

        public static void Tick() { 
            foreach(robot r in robot) {
                if (r != null)
                    r.Restart();
            }
        }
    }

    public class mapBlock {
        public point pozycja;
    }

    public class blockBorder : blockEmpty {
        public blockBorder(point p, System.Drawing.Color c):
            base(p,c){
        }
    }

    public class blockEmpty : mapBlock { public System.Drawing.Color color; public blockEmpty(point p, System.Drawing.Color color) { this.pozycja = p; this.color = color; } }

    public class blockSurface : mapBlock {  }

    public class blockResource : mapBlock { 
        public int ilosc;
        public blockResource(int ilosc)  { this.ilosc = ilosc; } 
    }

    public class blockObstacle : mapBlock {
        public int rodzaj = h.random.Next(0, 3);
 
    }

    public class blockRadioactive : mapBlock {
        public int rodzaj = 3;
        public int gestosc = h.random.Next(1, 20);
 
    }

    public class blockBase : mapBlock {
        public double obliczSileSygnalu(robotState state) {
            return h.obliczOdlegloscEuklidesowa(state.pozycja, this.pozycja);
        }
        public double obliczDystans(point nowaPozycja) {
            return h.obliczOdlegloscEuklidesowa(nowaPozycja, this.pozycja);
        }
 
    }

    public class mapData {
        public size rozmiar;
        public mapBlock[,] dane;
        public blockBase baza;

        static
        public mapData Generuj(size rozmiar) {
            mapData m = new mapData();
            m.baza = new blockBase();
            m.rozmiar = rozmiar;
            m.dane = new mapBlock[m.rozmiar.width, m.rozmiar.height];
            int r = 0;

            bool b = false;

            System.Drawing.Color colorA = System.Drawing.Color.DarkCyan;
            System.Drawing.Color colorB = System.Drawing.Color.LightCyan;
            System.Drawing.Color colorT;
            for (int i = 0; i < m.rozmiar.width; i++) {

                if(i % 5 == 0) { 
                    colorT = colorA;
                    colorA = colorB;
                    colorB = colorT;
                }

                m.dane[i, 0] = new blockBorder(new point(i, 0), colorB);
                m.dane[i, m.rozmiar.height-1] = new blockBorder(new point(i, m.rozmiar.height-1),colorA);
            }

            for(int i = 0; i < m.rozmiar.height; i++) {

                if(i % 5 == 0) {
                    colorT = colorB;
                    colorB = colorA;
                    colorA = colorT;
                }

                m.dane[0, i] = new blockBorder(new point(0, i), colorB);
                m.dane[m.rozmiar.width - 1, i] = new blockBorder(new point(m.rozmiar.width - 1, i), colorA);
            }

            for(int x = 1; x < m.rozmiar.width - 1; x++) {
                for(int y = 1; y < m.rozmiar.height - 1; y++) {
                    r = h.random.Next(0, 10);
                    point p = new point(x, y);
                    switch(r) {
                       // case 1: m.dane[x, y] = new blockRadioactive(); break;
                        case 2: m.dane[x, y] = new blockResource(h.random.Next(1000, 2000)); break;
                        case 3: m.dane[x, y] = new blockObstacle(); break;
                        default:
                            m.dane[x, y] = new blockSurface(); break;

                    }

                    m.dane[x, y].pozycja = p;

                }
            }

            m.baza.pozycja = new point(
                h.random.Next(5, m.rozmiar.width-5),
                h.random.Next(5, m.rozmiar.height-5)
                );

            foreach(point p in point.mozliweruchy) {
                point p2 = m.baza.pozycja + p;
                m.dane[p2.x, p2.y] = new blockSurface();
            }

            m.dane[m.baza.pozycja.x, m.baza.pozycja.y] = m.baza;
            m.dane[m.baza.pozycja.x - 1 , m.baza.pozycja.y] = m.baza;
            m.dane[m.baza.pozycja.x + 1 , m.baza.pozycja.y] = m.baza;
            m.dane[m.baza.pozycja.x , m.baza.pozycja.y - 1] = m.baza;
            m.dane[m.baza.pozycja.x , m.baza.pozycja.y + 1] = m.baza;

            return m;
        }
        public mapBlock this[point pozycja] {
            get { return sym.mapa.dane[pozycja.x, pozycja.y]; }
            set {
                value.pozycja = (point)pozycja.Clone();
                sym.mapa.dane[pozycja.x, pozycja.y] = value;
            }
        }

    }

    public class robotState {
        public point pozycja;
        public point kierunekRuchu;
        public int zebranyZasob;
        public Random random;
        public robotBehaviour zachowanie;
        public robotBehaviour zachowaniePoprzednie;
        public robotState() {
            pozycja = new point(0, 0);
            kierunekRuchu = new point(0, 0);
            zachowanie = new b4();

            random = new Random(h.random.Next(0, 1000000));

        }

    }

    public class robot {
        public int identyfikator;
        public robotState stan;
        
        public robot(int id) {
            identyfikator = id;
            stan = new robotState();
            task = new Task(() => run());
            stan.pozycja.x = stan.random.Next(1,  sym.mapa.rozmiar.width-1);
            stan.pozycja.y = stan.random.Next(1,  sym.mapa.rozmiar.width-1);

            sym.mapa[stan.pozycja] = new blockSurface();
            stan.kierunekRuchu.x = stan.random.Next(-1, 2);
            stan.kierunekRuchu.y = stan.random.Next(-1, 2);
            stan.zachowanie = sym.chain.behaviours[0];
        }
        public void Restart() { task = new Task(() => run()); task.Start(); }
        public Task task;
        private void run() {
            Thread t = Thread.CurrentThread;
            t.Name = this.identyfikator.ToString();

            robotBehaviour z = sym.chain.behaviours[0];

            Debug.Assert((sym.mapa[stan.pozycja] is blockObstacle) == false);

            while (!z.CzySpelnione(stan, sym.mapa)) {
                z = z.nextBehaviour;
            }

            stan.zachowaniePoprzednie = stan.zachowanie;
            stan.zachowanie = z;

            stan.zachowanie.SpelnijScenariusz(ref stan);

            Debug.Assert((sym.mapa[stan.pozycja] is blockObstacle) == false);

        }
    }

    public abstract class robotBehaviour {
        public abstract string id { get; }
        public abstract bool CzySpelnione(robotState state, mapData mapa);
        public abstract void SpelnijScenariusz(ref robotState stan);
        public robotBehaviour nextBehaviour;
        public override string ToString() {
            return id.ToString();
        }
    }

    public class chainBehaviour {
        public
        List<robotBehaviour> behaviours = new List<robotBehaviour>();

        public chainBehaviour() {


            behaviours.Add(new b0());
            behaviours.Add(new b1());
            behaviours.Add(new b2());
            behaviours.Add(new b3());
            behaviours.Add(new b4());
            behaviours.Add(new b5());

            int i = 0;
            for (;i< behaviours.Count - 1; i++) {
                behaviours[i].nextBehaviour = behaviours[i + 1];
            }

            behaviours[i].nextBehaviour = behaviours[0];
        }

    }
    public class b0 : robotBehaviour {
        public override string id { get { return "b0: Zmiana kierunku ruchu."; } }
        public override bool CzySpelnione(robotState stan, mapData mapa) {
            point npos = stan.pozycja + stan.kierunekRuchu;
            mapBlock block = sym.mapa.dane[npos.x, npos.y];

            if (block as blockSurface != null)
                return false;

            if (block as blockBorder != null)
                return true;

            if (block as blockBase != null)
                return true;

            return (block as blockObstacle != null);
        }
        public override void SpelnijScenariusz(ref robotState stan) {
            stan.kierunekRuchu = point.losuj(stan);
        }
    }

    public class b1: robotBehaviour {
        public override string id { get { return "b1: Baza: Zrzucenie ładunku."; } }
        public override bool CzySpelnione(robotState stan, mapData mapa) {

            double odl = h.obliczOdlegloscEuklidesowa(stan.pozycja, mapa.baza.pozycja);

            if (stan.zebranyZasob > 0 && odl < 1.5)
                return true;
            return false;
        }
        public override void SpelnijScenariusz(ref robotState stan) {
            stan.zebranyZasob = Math.Max(stan.zebranyZasob - 10, 0);
        }
    }

    public class b2: robotBehaviour {
        public override string id { get { return "b2: Powrót do bazy, gubienie próbek."; } }
        public override bool CzySpelnione(robotState stan, mapData mapa) {

            blockResource res = sym.mapa[stan.pozycja] as blockResource;

            /* jesli brak zasobu */
            if ((stan.zebranyZasob >= 100))
                return true;

            if (stan.zebranyZasob > 0 && res == null)
                return true;

            if (stan.zebranyZasob > 0 && res.ilosc == 0)
                return true;


            return false;
        }
        public override void SpelnijScenariusz(ref robotState stan) {
            /*
             * poszukaj drogi do bazy wg odleglosci
             * /
             */

            var block = sym.mapa[stan.pozycja];

            if (block is blockSurface) {
                block = new blockRadioactive() {
                    pozycja = (point)stan.pozycja.Clone()
                };
            }

            if (block is blockRadioactive) {
                var b = ((blockRadioactive)block); b.gestosc = Math.Min(200, b.gestosc + 2);
            }

            sym.mapa.dane[block.pozycja.x, block.pozycja.y] = block;

            stan.pozycja += stan.kierunekRuchu;

            point kierunek = point.krokWStroneBazy(stan.pozycja);
            stan.kierunekRuchu = kierunek;
            

        }
    }
    public class b3: robotBehaviour {
        public override string id { get { return "b3: Zbieranie zasobu"; } }
        public override bool CzySpelnione(robotState stan, mapData mapa ) {


            if (sym.mapa[stan.pozycja] is blockResource && stan.zebranyZasob < 100 ) {
                return true;
            }

            return false;
        }
        public override void SpelnijScenariusz(ref robotState stan) {

            blockResource res = sym.mapa[stan.pozycja] as blockResource;
            
            if(res!=null) {
                if (res.ilosc > 0) {
                    lock (sym.key) {
                        res.ilosc = res.ilosc - 10;
                        if (res.ilosc < 0)
                            stan.zebranyZasob = stan.zebranyZasob - Math.Abs(res.ilosc);
                    }
                    stan.zebranyZasob += 10;
                } 
                if (res.ilosc <= 0)
                    sym.mapa[res.pozycja] = new blockSurface() { pozycja = res.pozycja };
            }
        }
    }

    public class b4: robotBehaviour {
        public override string id =>  "b4: Podazanie wzdluz sciezki";
        public override bool CzySpelnione(robotState r, mapData m) {

            var block = sym.mapa[r.pozycja] as blockRadioactive;

            if (block != null) {
                return true;
            }

            return false;
        }
        public override void SpelnijScenariusz(ref robotState r) {

            var block = sym.mapa[r.pozycja];

            if (block is blockRadioactive) {
                var b = ((blockRadioactive)block); b.gestosc = Math.Max(0, b.gestosc - 1);
                if (b.gestosc == 0)
                    block = new blockSurface() { pozycja = (point)block.pozycja.Clone() };
            }
            sym.mapa[block.pozycja] = block;

            r.pozycja += r.kierunekRuchu;

            point kierunek = point.krokWzdluzSciezki(r.pozycja);
            r.kierunekRuchu = kierunek;
        }
    }

    public class b5: robotBehaviour {

        public override string id { get { return "b5: Eksploracja w losowym kierunku"; } }
        public override bool CzySpelnione(robotState stan, mapData mapa) {
            return true;
        }
        public override void SpelnijScenariusz(ref robotState stan) {
            stan.pozycja += stan.kierunekRuchu;
            stan.kierunekRuchu = point.losuj(stan);

        }
    }
}
