﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace wfaMarsExplorer {
    public partial class fMain : Form {

        public bool scaleSimDraw = true;
        public Timer timer = new Timer();

        private readonly int MAXDENSITY = 200;
        private readonly int XSIZE = 10;
        private readonly int YSIZE = 10;

        private readonly int XOFFSET = 10;
        private readonly int YOFFSET = 10;
        private readonly int YWINDOWOFFSET = 100; /* DODAC DO LISTY ROBOTOW */

        private readonly int WIDTH = (int)sym.mapa.rozmiar.width;
        private readonly int HEIGHT = (int)sym.mapa.rozmiar.height;

        private readonly Image mars = Image.FromFile(@"mars.jpg");
        //private readonly Image mars = new Bitmap(52, 52);

        public fMain() {

            InitializeComponent();

            this.timer.Interval = sym.predkosc;
            this.timer.Tick += timer_Tick;

            this.MinimumSize = new System.Drawing.Size(
                (XSIZE * sym.mapa.rozmiar.width) + 20 + XOFFSET + 200
                ,
                (YSIZE * sym.mapa.rozmiar.height) + 45 + YOFFSET 
                );
            this.Size = MinimumSize;
        }

        void timer_Tick(object sender, EventArgs e) {
            sym.Tick();
            Refresh();
        }

        public int cx(int x) { return XOFFSET + x * XSIZE; }
        public int cy(int y) { return (  sym.mapa.rozmiar.height*YSIZE) - y * YSIZE; }

        public void Rysuj(Graphics g) {

            Font font = new Font("Small Fonts", 5);

            float scaleX = (float)Height / (float)Math.Max(MinimumSize.Height, (HEIGHT * YSIZE));
            int d = 0;
            bool b = false;

            if (scaleSimDraw)
                g.ScaleTransform(
                    scaleX,
                    /*   (float)Width / (float)Math.Max(MinimumSize.Width, (WIDTH * XSIZE)), */ /* zachowanie proporcji */
                    scaleX);

            //g.SmoothingMode = SmoothingMode.HighQuality;
            g.DrawImage(mars, XOFFSET, YOFFSET, XSIZE * WIDTH, YSIZE * HEIGHT);

            for (int i = 0; i < sym.mapa.rozmiar.width; i++) {
                for (int j = 0; j < sym.mapa.rozmiar.height; j++) {
                    mapBlock mb = sym.mapa.dane[i, j];
                    if (mb is blockObstacle)
                        g.FillRectangle(Brushes.Red, cx(i), cy(j), XSIZE, YSIZE);
                    else if (mb is blockResource) {
                        g.FillRectangle(Brushes.Green, cx(i), cy(j), XSIZE, YSIZE);
                        g.DrawString((mb as blockResource).ilosc.ToString(), font, Brushes.Black, cx(i), cy(j));
                    } else if (mb is blockSurface)
                        ; //g.FillRectangle(Brushes.OrangeRed, cx(i), cy(j), XSIZE, YSIZE);
                    else if (mb is blockRadioactive) {
                        blockRadioactive br = (mb as blockRadioactive);
                        int gestosc = (255 * Math.Min(br.gestosc, MAXDENSITY)) / MAXDENSITY;

                        Brush przezroczysty = new SolidBrush(Color.FromArgb(gestosc, Color.DarkBlue));

                        g.FillRectangle(przezroczysty, cx(i), cy(j), XSIZE, YSIZE);
                    } else if (mb is blockEmpty) {
                        g.FillRectangle(new SolidBrush((mb as blockEmpty).color), cx(i), cy(j), XSIZE, YSIZE);
                        g.DrawRectangle(Pens.Black, cx(i), cy(j), XSIZE, YSIZE);
                    } else {
                        //
                    }

                }
            }


            foreach (robot r in sym.robot) {
                if (r != null) {

                    g.FillEllipse(Brushes.Pink, cx(r.stan.pozycja.x), cy(r.stan.pozycja.y), XSIZE, YSIZE);
                    g.DrawEllipse(Pens.Black, cx(r.stan.pozycja.x), cy(r.stan.pozycja.y), XSIZE, YSIZE);
                    g.DrawString(r.identyfikator.ToString(), font, Brushes.Black, cx(r.stan.pozycja.x), cy(r.stan.pozycja.y));
                }

            }


            GraphicsPath path = new GraphicsPath();
            path.AddEllipse(cx(sym.mapa.baza.pozycja.x - 1), cy(sym.mapa.baza.pozycja.y + 1), 3 * XSIZE, 3 * YSIZE);

            PathGradientBrush gradient = new PathGradientBrush(path);
            gradient.CenterColor = Color.Black;
            gradient.SurroundColors = new Color[] { Color.Gray};

            Brush basePrzezroczysty = new SolidBrush(Color.FromArgb(128, Color.Black));
            g.FillEllipse(gradient, cx(sym.mapa.baza.pozycja.x-1), cy( sym.mapa.baza.pozycja.y+1), 3 * XSIZE, 3 * YSIZE);
            g.DrawEllipse(Pens.Black, cx(sym.mapa.baza.pozycja.x - 1), cy(sym.mapa.baza.pozycja.y + 1), 3 * XSIZE, 3 * YSIZE);
         //   g.FillRectangle(Brushes.Black, cx(i), cy(j), XSIZE, YSIZE);


            g.ResetTransform();

            g.DrawString("Status: ", SystemFonts.DefaultFont, Brushes.SaddleBrown,20 +  WIDTH*YSIZE*scaleX, 5);

            foreach(robot r in sym.robot) {
                g.DrawString(r.identyfikator.ToString("000") + " => " + r.stan.zachowanie.ToString(), SystemFonts.DefaultFont, Brushes.Black, 20 + WIDTH * YSIZE * scaleX, 30 + (10 * r.identyfikator));
            }

        }

        private void fMain_Load(object sender, EventArgs e) {

        }
        protected override void OnPaint(PaintEventArgs e) {
            base.OnPaint(e);
            Rysuj(e.Graphics);
        }

        private void fMain_Resize(object sender, EventArgs e) {
            Refresh();
        }

        private void button1_Click(object sender, EventArgs e) {

            if(!timer.Enabled)
                timer.Start();
            else
                timer.Stop();
        }
    }
}
