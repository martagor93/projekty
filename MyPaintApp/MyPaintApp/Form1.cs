﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using System.Globalization;
using System.Drawing.Drawing2D;
using System.Resources;
using System.Reflection;
using System.Drawing.Imaging;
using IPlugin;
using System.IO;
using System.Threading.Tasks;

namespace MyPaintApp
{
    public partial class Form1 : Form
    {
        private Stack<Image> Undo = new Stack<Image>();
        private Stack<Image> Redo = new Stack<Image>();

        public List<IPluginInterface> plugins = new List<IPluginInterface>();

        private readonly object undoRedo = new object();
        Graphics drawing;
        bool startDrawing = false;
        int? X = null;
        int? Y = null;
        int Xr;
        int Yr;
        int linestyle=1;
        int choosenShape;
        int difX, difY;
        public Form1()
        {
            InitializeComponent();
            drawing = pnlDrawing.CreateGraphics();
            pnlDrawing.Image = new Bitmap(pnlDrawing.Width, pnlDrawing.Height);
        }
        private void pnlDrawing_MouseDown(object sender, MouseEventArgs e)
        {
            startDrawing = true;
            switch (choosenShape)
            {
                case 1:
                    Xr = e.X;
                    Yr = e.Y;
                    break;
                case 2:
                    Xr = e.X;
                    Yr = e.Y;
                    break;
                case 3:
                    Xr = e.X;
                    Yr = e.Y;
                    break;
            }
           

        }
        private void pnlDrawing_MouseMove(object sender, MouseEventArgs e)
        {
            if (startDrawing == true && choosenShape == 4 && e.Button == MouseButtons.Left)
            {

                if (linestyle == 2)
                {
                    UpdateImage(delegate ()
                    {
                        Bitmap bmp = new Bitmap(pnlDrawing.Image);
                        drawing = Graphics.FromImage(bmp);
                        Pen dash_pen = new Pen(btnColor.BackColor, int.Parse(penWidth.Text));
                        dash_pen.DashStyle = DashStyle.Dash;
                        drawing.DrawLine(dash_pen, new Point(X ?? e.X, Y ?? e.Y), new Point(e.X, e.Y));
                        X = e.X;
                        Y = e.Y;
                        pnlDrawing.Image = bmp;
                    });
                }
                else if (linestyle == 3)
                {
                    Bitmap bmp = new Bitmap(pnlDrawing.Image);
                    drawing = Graphics.FromImage(bmp);
                    Pen dashDot_pen = new Pen(btnColor.BackColor, int.Parse(penWidth.Text));
                    dashDot_pen.DashStyle = DashStyle.DashDot;
                    drawing.DrawLine(dashDot_pen, new Point(X ?? e.X, Y ?? e.Y), new Point(e.X, e.Y));
                    X = e.X;
                    Y = e.Y;
                    pnlDrawing.Image = bmp;
                }
                else if (linestyle == 4)
                {
                    UpdateImage(delegate ()
                    {
                        Bitmap bmp = new Bitmap(pnlDrawing.Image);
                        drawing = Graphics.FromImage(bmp);
                        Pen Dot_pen = new Pen(btnColor.BackColor, int.Parse(penWidth.Text));
                        Dot_pen.DashStyle = DashStyle.Dot;
                        drawing.DrawLine(Dot_pen, new Point(X ?? e.X, Y ?? e.Y), new Point(e.X, e.Y));
                        X = e.X;
                        Y = e.Y;
                        pnlDrawing.Image = bmp;
                    });
                }
                else
                {
                    UpdateImage(delegate ()
                    {
                        Bitmap bmp = new Bitmap(pnlDrawing.Image);
                        drawing = Graphics.FromImage(bmp);
                        Pen pen = new Pen(btnColor.BackColor, int.Parse(penWidth.Text));
                        drawing.DrawLine(pen, new Point(X ?? e.X, Y ?? e.Y), new Point(e.X, e.Y));
                        X = e.X;
                        Y = e.Y;
                        pnlDrawing.Image = bmp;
                    });
                }
            }
        }

        private void pnlDrawing_MouseUp(object sender, MouseEventArgs e)
        {

            switch (choosenShape)
            {
                case 1:
                    if (linestyle == 2)
                    {
                        UpdateImage(delegate ()
                        {
                            Bitmap bmp = new Bitmap(pnlDrawing.Image);
                            drawing = Graphics.FromImage(bmp);
                            difX = e.X - Xr;
                            difY = e.Y - Yr;
                            Pen dash_pen = new Pen(btnColor.BackColor, int.Parse(penWidth.Text));
                            drawing.DrawLine(dash_pen, Xr, Yr, e.X, e.Y);
                            pnlDrawing.Image = bmp;
                        });
                    }
                    else if (linestyle == 3)
                    {
                        UpdateImage(delegate ()
                        {
                            Bitmap bmp = new Bitmap(pnlDrawing.Image);
                            drawing = Graphics.FromImage(bmp);
                            difX = e.X - Xr;
                            difY = e.Y - Yr;
                            Pen dashDot_pen = new Pen(btnColor.BackColor, int.Parse(penWidth.Text));
                            dashDot_pen.DashStyle = DashStyle.DashDot;
                            drawing.DrawLine(dashDot_pen, Xr, Yr, e.X, e.Y);
                            pnlDrawing.Image = bmp;
                        });
                    }
                    else if (linestyle == 4)
                    {
                        UpdateImage(delegate ()
                        {
                            Bitmap bmp = new Bitmap(pnlDrawing.Image);
                            drawing = Graphics.FromImage(bmp);
                            difX = e.X - Xr;
                            difY = e.Y - Yr;
                            Pen Dot_pen = new Pen(btnColor.BackColor, int.Parse(penWidth.Text));
                            Dot_pen.DashStyle = DashStyle.Dot;
                            drawing.DrawLine(Dot_pen, Xr, Yr, e.X, e.Y);
                            pnlDrawing.Image = bmp;
                        });
                    }
                    else
                    {
                        UpdateImage(delegate ()
                        {
                            Bitmap bmp = new Bitmap(pnlDrawing.Image);
                            drawing = Graphics.FromImage(bmp);
                            difX = e.X - Xr;
                            difY = e.Y - Yr;
                            Pen pen = new Pen(btnColor.BackColor, int.Parse(penWidth.Text));
                            drawing.DrawLine(pen, Xr, Yr, e.X, e.Y);
                            pnlDrawing.Image = bmp;
                        });
                    }
                    break;
                case 2:
                    if (linestyle == 2)
                    {
                        UpdateImage(delegate ()
                        {
                            Bitmap bmp = new Bitmap(pnlDrawing.Image);
                            drawing = Graphics.FromImage(bmp);
                            difX = e.X - Xr;
                            difY = e.Y - Yr;
                            Pen dash_pen = new Pen(btnColor.BackColor, int.Parse(penWidth.Text));
                            dash_pen.DashStyle = DashStyle.Dash;
                            drawing.DrawRectangle(dash_pen, Xr, Yr, difX, difY);
                            pnlDrawing.Image = bmp;
                        });
                    }
                    else if (linestyle == 3)
                    {
                        UpdateImage(delegate ()
                        {
                            Bitmap bmp = new Bitmap(pnlDrawing.Image);
                            drawing = Graphics.FromImage(bmp);
                            difX = e.X - Xr;
                            difY = e.Y - Yr;
                            Pen dashDot_pen = new Pen(btnColor.BackColor, int.Parse(penWidth.Text));
                            dashDot_pen.DashStyle = DashStyle.DashDot;
                            drawing.DrawRectangle(dashDot_pen, Xr, Yr, difX, difY);
                            pnlDrawing.Image = bmp;
                        });
                    }
                    else if (linestyle == 4)
                    {
                        UpdateImage(delegate ()
                        {
                            Bitmap bmp = new Bitmap(pnlDrawing.Image);
                            drawing = Graphics.FromImage(bmp);
                            difX = e.X - Xr;
                            difY = e.Y - Yr;
                            Pen Dot_pen = new Pen(btnColor.BackColor, int.Parse(penWidth.Text));
                            Dot_pen.DashStyle = DashStyle.Dot;
                            drawing.DrawRectangle(Dot_pen, Xr, Yr, difX, difY);
                            pnlDrawing.Image = bmp;
                        });
                    }
                    else
                    {
                        UpdateImage(delegate ()
                        {
                            Bitmap bmp = new Bitmap(pnlDrawing.Image);
                            drawing = Graphics.FromImage(bmp);
                            difX = e.X - Xr;
                            difY = e.Y - Yr;
                            Pen pen = new Pen(btnColor.BackColor, int.Parse(penWidth.Text));
                            drawing.DrawRectangle(pen, Xr, Yr, difX, difY);
                            pnlDrawing.Image = bmp;
                        });
                    }
                    break;
                case 3:
                    if (linestyle == 2)
                    {
                        UpdateImage(delegate ()
                        {
                            Bitmap bmp = new Bitmap(pnlDrawing.Image);
                            drawing = Graphics.FromImage(bmp);
                            difX = e.X - Xr;
                            difY = e.Y - Yr;
                            Pen dash_pen = new Pen(btnColor.BackColor, int.Parse(penWidth.Text));
                            dash_pen.DashStyle = DashStyle.Dash;
                            drawing.DrawRectangle(dash_pen, Xr, Yr, difX, difY);
                            pnlDrawing.Image = bmp;
                        });
                    }
                    else if (linestyle == 3)
                    {
                        UpdateImage(delegate ()
                        {
                            Bitmap bmp = new Bitmap(pnlDrawing.Image);
                            drawing = Graphics.FromImage(bmp);
                            difX = e.X - Xr;
                            difY = e.Y - Yr;
                            Pen dashDot_pen = new Pen(btnColor.BackColor, int.Parse(penWidth.Text));
                            dashDot_pen.DashStyle = DashStyle.DashDot;
                            drawing.DrawEllipse(dashDot_pen, Xr, Yr, difX, difY);
                            pnlDrawing.Image = bmp;
                        });
                    }
                    else if (linestyle == 4)
                    {
                        UpdateImage(delegate ()
                        {
                            Bitmap bmp = new Bitmap(pnlDrawing.Image);
                            drawing = Graphics.FromImage(bmp);
                            difX = e.X - Xr;
                            difY = e.Y - Yr;
                            Pen Dot_pen = new Pen(btnColor.BackColor, int.Parse(penWidth.Text));
                            Dot_pen.DashStyle = DashStyle.Dot;
                            drawing.DrawEllipse(Dot_pen, Xr, Yr, difX, difY);
                            pnlDrawing.Image = bmp;
                        });
                    }
                    else
                    {
                        UpdateImage(delegate ()
                        {
                            Bitmap bmp = new Bitmap(pnlDrawing.Image);
                            drawing = Graphics.FromImage(bmp);
                            difX = e.X - Xr;
                            difY = e.Y - Yr;
                            Pen pen = new Pen(btnColor.BackColor, int.Parse(penWidth.Text));
                            drawing.DrawEllipse(pen, Xr, Yr, difX, difY);
                            pnlDrawing.Image = bmp;
                        });
                    }
                    break;
             }             
                startDrawing = false;
                X = null;
                Y = null;
            }

        private void btnColor_Click(object sender, EventArgs e)
        {
            ColorDialog color = new ColorDialog();
            if (color.ShowDialog() == DialogResult.OK)
            {
                btnColor.BackColor = color.Color;
            }
        }

        private void btnLine_Click(object sender, EventArgs e)
        {
           choosenShape = 1;   
        }

        private void btnRectangle_Click(object sender, EventArgs e)
        {
            choosenShape = 2;
        }

        private void btnCircle_Click(object sender, EventArgs e)
        {
           choosenShape = 3;
        }

        private void newMenu_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Do you want to save changes?", "MyPaint", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "JPG *.jpg  | *.jpg | BMP (*.bmp) | *.bmp | PNG (*png) | *.png";
                sfd.Title = "Save an image file";
                sfd.ShowDialog();
                if (sfd.FileName != "")
                {
                    switch(sfd.FilterIndex)
                    {
                        case 1:
                            int width = Convert.ToInt32(pnlDrawing.Width);
                            int height = Convert.ToInt32(pnlDrawing.Height);
                            Bitmap bmp = new Bitmap(width, height);
                            pnlDrawing.DrawToBitmap(bmp, new Rectangle(0, 0, width, height));
                            bmp.Save(sfd.FileName, ImageFormat.Jpeg);
                            break;
                        case 2:
                            int width2 = Convert.ToInt32(pnlDrawing.Width);
                            int height2 = Convert.ToInt32(pnlDrawing.Height);
                            Bitmap bmp1 = new Bitmap(width2, height2);
                            pnlDrawing.DrawToBitmap(bmp1, new Rectangle(0, 0, width2, height2));
                            bmp1.Save(sfd.FileName, ImageFormat.Bmp);
                            break;
                        case 3:
                            int width3 = Convert.ToInt32(pnlDrawing.Width);
                            int height3 = Convert.ToInt32(pnlDrawing.Height);
                            Bitmap bmp3 = new Bitmap(width3, height3);
                            pnlDrawing.DrawToBitmap(bmp3, new Rectangle(0, 0, width3, height3));
                            bmp3.Save(sfd.FileName, ImageFormat.Png);
                            break;
                    } 
                    drawing.Clear(Color.White);

                }

            }
            if (result == DialogResult.No)
            {
                pnlDrawing.Image = null;
                pnlDrawing.Refresh();
            }
        }
        private void openMenu_Click(object sender, EventArgs e)
        {
            OpenFileDialog opd = new OpenFileDialog();
            opd.Filter = "JPG (*.jpg)  | *.jpg| BMP (*.bmp) | *.bmp| PNG (*png) | *.png";
            opd.Title = "Open image";
            DialogResult open = opd.ShowDialog();
            if (open == DialogResult.OK)
            {
                pnlDrawing.Image = new Bitmap(opd.FileName);
            }
            opd.Dispose();
        }

        private void saveAsMenu_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "JPG (*.jpg)  | *.jpg| BMP (*.bmp) | *.bmp| PNG (*png) | *.png";
            sfd.Title = "Save an image file";
            DialogResult save = sfd.ShowDialog();
            if (save == DialogResult.OK)
            {             
                if (sfd.FileName != "")
                {
                    switch(sfd.FilterIndex)
                    {
                        case 1:
                            int width = Convert.ToInt32(pnlDrawing.Width);
                            int height = Convert.ToInt32(pnlDrawing.Height);
                            Bitmap bmp = new Bitmap(width, height);
                            pnlDrawing.DrawToBitmap(bmp, new Rectangle(0, 0, width, height));
                            bmp.Save(sfd.FileName, ImageFormat.Jpeg);
                            break;
                        case 2:
                            int width2 = Convert.ToInt32(pnlDrawing.Width);
                            int height2 = Convert.ToInt32(pnlDrawing.Height);
                            Bitmap bmp1 = new Bitmap(width2, height2);
                            pnlDrawing.DrawToBitmap(bmp1, new Rectangle(0, 0, width2, height2));
                            bmp1.Save(sfd.FileName, ImageFormat.Bmp);
                            break;
                        case 3:
                            int width3 = Convert.ToInt32(pnlDrawing.Width);
                            int height3 = Convert.ToInt32(pnlDrawing.Height);
                            Bitmap bmp3 = new Bitmap(width3, height3);
                            pnlDrawing.DrawToBitmap(bmp3, new Rectangle(0, 0, width3, height3));
                            bmp3.Save(sfd.FileName, ImageFormat.Png);
                            break;
                    }                    
                }
            }
            sfd.Dispose();
        }
        private void exitMenu_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Do you want to save changes?", "MyPaint", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "JPG (*.jpg)  | *.jpg | BMP (*.bmp) | *.bmp | PNG (*png) | *.png";
                sfd.Title = "Save an image file";
                sfd.ShowDialog();
                if (sfd.FileName != "")
                {
                    switch (sfd.FilterIndex)
                    {
                        case 1:
                            int width = Convert.ToInt32(pnlDrawing.Width);
                            int height = Convert.ToInt32(pnlDrawing.Height);
                            Bitmap bmp = new Bitmap(width, height);
                            pnlDrawing.DrawToBitmap(bmp, new Rectangle(0, 0, width, height));
                            bmp.Save(sfd.FileName, ImageFormat.Jpeg);
                            break;
                        case 2:
                            int width2 = Convert.ToInt32(pnlDrawing.Width);
                            int height2 = Convert.ToInt32(pnlDrawing.Height);
                            Bitmap bmp1 = new Bitmap(width2, height2);
                            pnlDrawing.DrawToBitmap(bmp1, new Rectangle(0, 0, width2, height2));
                            bmp1.Save(sfd.FileName, ImageFormat.Bmp);
                            break;
                        case 3:
                            int width3 = Convert.ToInt32(pnlDrawing.Width);
                            int height3 = Convert.ToInt32(pnlDrawing.Height);
                            Bitmap bmp3 = new Bitmap(width3, height3);
                            pnlDrawing.DrawToBitmap(bmp3, new Rectangle(0, 0, width3, height3));
                            bmp3.Save(sfd.FileName, ImageFormat.Png);
                            break;
                    }
                    pnlDrawing.Image = null;
                    pnlDrawing.Refresh();
                }

            }
            if (result == DialogResult.No)
            {
                Close();
            }
            Close();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            pnlDrawing.Image = null;
            pnlDrawing.Refresh();
        }

        private void polishMenu_Click(object sender, EventArgs e)
        {
            CultureInfo ci = new CultureInfo("pl-PL");
            Assembly a = Assembly.Load("MyPaintApp");
            ResourceManager rm = new ResourceManager("MyPaintApp.Language.language-pl", a);
            fileMenu.Text = rm.GetString("fileMenu", ci);
            newMenu.Text = rm.GetString("newMenu", ci);
            openMenu.Text = rm.GetString("openMenu", ci);
            saveAsMenu.Text = rm.GetString("saveAsMenu", ci);
            exitMenu.Text = rm.GetString("exitMenu", ci);
            editMenu.Text = rm.GetString("editMenu", ci);
            undoMenu.Text = rm.GetString("undoMenu", ci);
            redoMenu.Text = rm.GetString("redoMenu", ci);
            languageMenu.Text = rm.GetString("languageMenu", ci);
            polishMenu.Text = rm.GetString("polishMenu", ci);
            englishMenu.Text = rm.GetString("englishMenu", ci);
            deutschMenu.Text = rm.GetString("deutschMenu", ci);
            aboutMenu.Text = rm.GetString("aboutMenu", ci);
            btnClear.Text = rm.GetString("btnClear", ci);
            colorBox.Text = rm.GetString("colorBox", ci);
            linestyleBox.Text = rm.GetString("linestyleBox", ci);
            shapesBox.Text = rm.GetString("shapesBox", ci);
            drawingBox.Text = rm.GetString("drawingBox", ci);
            lineBox.Text = rm.GetString("lineBox", ci);
        }

        private void englishMenu_Click(object sender, EventArgs e)
        {
            CultureInfo ci = new CultureInfo("en-GB");
            Assembly a = Assembly.Load("MyPaintApp");
            ResourceManager rm = new ResourceManager("MyPaintApp.Language.language-eng", a);
            fileMenu.Text = rm.GetString("fileMenu", ci);
            newMenu.Text = rm.GetString("newMenu", ci);
            openMenu.Text = rm.GetString("openMenu", ci);
            saveAsMenu.Text = rm.GetString("saveAsMenu", ci);
            exitMenu.Text = rm.GetString("exitMenu", ci);
            editMenu.Text = rm.GetString("editMenu", ci);
            undoMenu.Text = rm.GetString("undoMenu", ci);
            redoMenu.Text = rm.GetString("redoMenu", ci);
            languageMenu.Text = rm.GetString("languageMenu", ci);
            polishMenu.Text = rm.GetString("polishMenu", ci);
            englishMenu.Text = rm.GetString("englishMenu", ci);
            deutschMenu.Text = rm.GetString("deutschMenu", ci);
            aboutMenu.Text = rm.GetString("aboutMenu", ci);
            btnClear.Text = rm.GetString("btnClear", ci);
            colorBox.Text = rm.GetString("colorBox", ci);
            linestyleBox.Text = rm.GetString("linestyleBox", ci);
            shapesBox.Text = rm.GetString("shapesBox", ci);
            drawingBox.Text = rm.GetString("drawingBox", ci);
            lineBox.Text = rm.GetString("lineBox", ci);
        }


        private void deutschMenu_Click(object sender, EventArgs e)
        {
            CultureInfo ci = new CultureInfo("de-De");
            Assembly a = Assembly.Load("MyPaintApp");
            ResourceManager rm = new ResourceManager("MyPaintApp.Language.language-de", a);
            fileMenu.Text = rm.GetString("fileMenu", ci);
            newMenu.Text = rm.GetString("newMenu", ci);
            openMenu.Text = rm.GetString("openMenu", ci);
            saveAsMenu.Text = rm.GetString("saveAsMenu", ci);
            exitMenu.Text = rm.GetString("exitMenu", ci);
            editMenu.Text = rm.GetString("editMenu", ci);
            undoMenu.Text = rm.GetString("undoMenu", ci);
            redoMenu.Text = rm.GetString("redoMenu", ci);
            languageMenu.Text = rm.GetString("languageMenu", ci);
            polishMenu.Text = rm.GetString("polishMenu", ci);
            englishMenu.Text = rm.GetString("englishMenu", ci);
            deutschMenu.Text = rm.GetString("deutschMenu", ci);
            aboutMenu.Text = rm.GetString("aboutMenu", ci);
            btnClear.Text = rm.GetString("btnClear", ci);
            colorBox.Text = rm.GetString("colorBox", ci);
            linestyleBox.Text = rm.GetString("linestyleBox", ci);
            shapesBox.Text = rm.GetString("shapesBox", ci);
            drawingBox.Text = rm.GetString("drawingBox", ci);
            lineBox.Text = rm.GetString("lineBox", ci);
        }
        private void UndoOperation()
        {
            lock (undoRedo)
            {
                if (Undo.Count > 0)
                {
                    Redo.Push(Undo.Pop());
                    pnlDrawing.Image = Redo.Peek();
                    pnlDrawing.Refresh();
                }

            }
        }
        private void undoMenu_Click(object sender, EventArgs e)
        {
            UndoOperation();
        }
        private void RedoOperation()
        {
            lock (undoRedo)
            {
                if (Redo.Count != 0)
                {
                    Undo.Push(Redo.Pop());
                    pnlDrawing.Image = Undo.Peek();
                    pnlDrawing.Refresh();
                }
            }
        }
        async Task<Image> TransformationAsync(object bitmap)
        {
            string pluginPath = Path.Combine(Environment.CurrentDirectory, "Plugins\\SepiaPlugin.dll");
            Assembly sepiaAsm = Assembly.LoadFrom(pluginPath);
            if(sepiaAsm!=null)
            {
                Type pluginType = null;
                foreach (var t in sepiaAsm.ExportedTypes)
                {
                    if(typeof(IPluginInterface).IsAssignableFrom(t))
                    {
                        pluginType = t;
                        break;
                    }
                }

                if (pluginType != null)
                {
                    IPluginInterface plugin = (IPluginInterface)Activator.CreateInstance(pluginType);
                    Bitmap bmp = (Bitmap)bitmap;
                    return await Task.Run<Image>(() => plugin.transformImage(bmp));
                }
            }
            throw new Exception("Error loading assembly");            
        }

        private void btnSepia_Click(object sender, EventArgs e)
        {
            int width = Convert.ToInt32(pnlDrawing.Width);
            int height = Convert.ToInt32(pnlDrawing.Height);
            Bitmap bmp3 = new Bitmap(width, height);
            pnlDrawing.DrawToBitmap(bmp3, new Rectangle(0, 0, width, height));            

            Task<Image> task = TransformationAsync(bmp3);
            
            task.ContinueWith((img) =>
            {                
                MessageBox.Show("koniec sepii");
                pnlDrawing.Image = img.Result;
                
            });
            
            
        }

        private void pencilBtn_Click(object sender, EventArgs e)
        {
            choosenShape = 4;
        }
        private void redoMenu_Click(object sender, EventArgs e)
        {
            RedoOperation();
        }

        private void aboutMenu_Click(object sender, EventArgs e)
        {
            Thread t2 = new Thread(new ThreadStart(ThreadProc));
            t2.Start();
        }
        private void ThreadProc()
        {
            var about = new MyPaintApp.About();
            about.ShowDialog();
        }

        private void solidPnl_Click(object sender, EventArgs e)
        {

            linestyle = 1;
        }

        private void dashPnl_Click(object sender, EventArgs e)
        {
            linestyle = 2;
        }

        private void dashdotPnl_Click(object sender, EventArgs e)
        {
            linestyle = 3;
        }

        private void dotPnl_Click(object sender, EventArgs e)
        {
            linestyle = 4;
        }

        public void UpdateImage(Action updateImage)
        {
            lock (undoRedo)
            {
                Undo.Push(pnlDrawing.Image);

                try
                {
                    updateImage();
                }
                catch
                {
                    Undo.Pop();
                    throw;
                }
            }
        }
    }
}