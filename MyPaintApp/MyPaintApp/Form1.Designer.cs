﻿namespace MyPaintApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.newMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.openMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.exitMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.undoMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.redoMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.languageMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.polishMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.englishMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.deutschMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.btnColor = new System.Windows.Forms.Button();
            this.btnLine = new System.Windows.Forms.Button();
            this.btnRectangle = new System.Windows.Forms.Button();
            this.btnCircle = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.penWidth = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.drawingBox = new System.Windows.Forms.GroupBox();
            this.pencilBtn = new System.Windows.Forms.Button();
            this.colorBox = new System.Windows.Forms.GroupBox();
            this.lineBox = new System.Windows.Forms.GroupBox();
            this.linestyleBox = new System.Windows.Forms.GroupBox();
            this.dotPnl = new System.Windows.Forms.Panel();
            this.dashdotPnl = new System.Windows.Forms.Panel();
            this.dashPnl = new System.Windows.Forms.Panel();
            this.solidPnl = new System.Windows.Forms.Panel();
            this.shapesBox = new System.Windows.Forms.GroupBox();
            this.pnlDrawing = new System.Windows.Forms.PictureBox();
            this.btnSepia = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.drawingBox.SuspendLayout();
            this.colorBox.SuspendLayout();
            this.lineBox.SuspendLayout();
            this.linestyleBox.SuspendLayout();
            this.shapesBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDrawing)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.editMenu,
            this.languageMenu,
            this.aboutMenu});
            resources.ApplyResources(this.menuStrip1, "menuStrip1");
            this.menuStrip1.Name = "menuStrip1";
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newMenu,
            this.openMenu,
            this.saveAsMenu,
            this.exitMenu});
            this.fileMenu.Name = "fileMenu";
            resources.ApplyResources(this.fileMenu, "fileMenu");
            // 
            // newMenu
            // 
            this.newMenu.Name = "newMenu";
            resources.ApplyResources(this.newMenu, "newMenu");
            this.newMenu.Click += new System.EventHandler(this.newMenu_Click);
            // 
            // openMenu
            // 
            this.openMenu.Name = "openMenu";
            resources.ApplyResources(this.openMenu, "openMenu");
            this.openMenu.Click += new System.EventHandler(this.openMenu_Click);
            // 
            // saveAsMenu
            // 
            this.saveAsMenu.Name = "saveAsMenu";
            resources.ApplyResources(this.saveAsMenu, "saveAsMenu");
            this.saveAsMenu.Click += new System.EventHandler(this.saveAsMenu_Click);
            // 
            // exitMenu
            // 
            this.exitMenu.Name = "exitMenu";
            resources.ApplyResources(this.exitMenu, "exitMenu");
            this.exitMenu.Click += new System.EventHandler(this.exitMenu_Click);
            // 
            // editMenu
            // 
            this.editMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoMenu,
            this.redoMenu});
            this.editMenu.Name = "editMenu";
            resources.ApplyResources(this.editMenu, "editMenu");
            // 
            // undoMenu
            // 
            this.undoMenu.Name = "undoMenu";
            resources.ApplyResources(this.undoMenu, "undoMenu");
            this.undoMenu.Click += new System.EventHandler(this.undoMenu_Click);
            // 
            // redoMenu
            // 
            this.redoMenu.Name = "redoMenu";
            resources.ApplyResources(this.redoMenu, "redoMenu");
            this.redoMenu.Click += new System.EventHandler(this.redoMenu_Click);
            // 
            // languageMenu
            // 
            this.languageMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.polishMenu,
            this.englishMenu,
            this.deutschMenu});
            this.languageMenu.Name = "languageMenu";
            resources.ApplyResources(this.languageMenu, "languageMenu");
            // 
            // polishMenu
            // 
            this.polishMenu.Name = "polishMenu";
            resources.ApplyResources(this.polishMenu, "polishMenu");
            this.polishMenu.Click += new System.EventHandler(this.polishMenu_Click);
            // 
            // englishMenu
            // 
            this.englishMenu.Name = "englishMenu";
            resources.ApplyResources(this.englishMenu, "englishMenu");
            this.englishMenu.Click += new System.EventHandler(this.englishMenu_Click);
            // 
            // deutschMenu
            // 
            this.deutschMenu.Name = "deutschMenu";
            resources.ApplyResources(this.deutschMenu, "deutschMenu");
            this.deutschMenu.Click += new System.EventHandler(this.deutschMenu_Click);
            // 
            // aboutMenu
            // 
            this.aboutMenu.Name = "aboutMenu";
            resources.ApplyResources(this.aboutMenu, "aboutMenu");
            this.aboutMenu.Click += new System.EventHandler(this.aboutMenu_Click);
            // 
            // btnColor
            // 
            this.btnColor.BackColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.btnColor, "btnColor");
            this.btnColor.Name = "btnColor";
            this.btnColor.UseVisualStyleBackColor = false;
            this.btnColor.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // btnLine
            // 
            resources.ApplyResources(this.btnLine, "btnLine");
            this.btnLine.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnLine.Name = "btnLine";
            this.btnLine.UseVisualStyleBackColor = true;
            this.btnLine.Click += new System.EventHandler(this.btnLine_Click);
            // 
            // btnRectangle
            // 
            resources.ApplyResources(this.btnRectangle, "btnRectangle");
            this.btnRectangle.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnRectangle.Name = "btnRectangle";
            this.btnRectangle.UseVisualStyleBackColor = true;
            this.btnRectangle.Click += new System.EventHandler(this.btnRectangle_Click);
            // 
            // btnCircle
            // 
            resources.ApplyResources(this.btnCircle, "btnCircle");
            this.btnCircle.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnCircle.Name = "btnCircle";
            this.btnCircle.UseVisualStyleBackColor = true;
            this.btnCircle.Click += new System.EventHandler(this.btnCircle_Click);
            // 
            // btnClear
            // 
            resources.ApplyResources(this.btnClear, "btnClear");
            this.btnClear.Name = "btnClear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // penWidth
            // 
            this.penWidth.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.penWidth, "penWidth");
            this.penWidth.FormattingEnabled = true;
            this.penWidth.Items.AddRange(new object[] {
            resources.GetString("penWidth.Items"),
            resources.GetString("penWidth.Items1"),
            resources.GetString("penWidth.Items2"),
            resources.GetString("penWidth.Items3")});
            this.penWidth.Name = "penWidth";
            // 
            // panel2
            // 
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.panel2.Controls.Add(this.btnSepia);
            this.panel2.Controls.Add(this.drawingBox);
            this.panel2.Controls.Add(this.colorBox);
            this.panel2.Controls.Add(this.lineBox);
            this.panel2.Controls.Add(this.linestyleBox);
            this.panel2.Controls.Add(this.shapesBox);
            this.panel2.Controls.Add(this.btnClear);
            this.panel2.Name = "panel2";
            // 
            // drawingBox
            // 
            this.drawingBox.BackColor = System.Drawing.Color.SkyBlue;
            this.drawingBox.Controls.Add(this.pencilBtn);
            resources.ApplyResources(this.drawingBox, "drawingBox");
            this.drawingBox.Name = "drawingBox";
            this.drawingBox.TabStop = false;
            // 
            // pencilBtn
            // 
            resources.ApplyResources(this.pencilBtn, "pencilBtn");
            this.pencilBtn.Cursor = System.Windows.Forms.Cursors.Default;
            this.pencilBtn.Name = "pencilBtn";
            this.pencilBtn.UseVisualStyleBackColor = true;
            this.pencilBtn.Click += new System.EventHandler(this.pencilBtn_Click);
            // 
            // colorBox
            // 
            this.colorBox.BackColor = System.Drawing.Color.SkyBlue;
            this.colorBox.Controls.Add(this.btnColor);
            resources.ApplyResources(this.colorBox, "colorBox");
            this.colorBox.Name = "colorBox";
            this.colorBox.TabStop = false;
            // 
            // lineBox
            // 
            this.lineBox.BackColor = System.Drawing.Color.SkyBlue;
            this.lineBox.Controls.Add(this.penWidth);
            resources.ApplyResources(this.lineBox, "lineBox");
            this.lineBox.Name = "lineBox";
            this.lineBox.TabStop = false;
            // 
            // linestyleBox
            // 
            this.linestyleBox.BackColor = System.Drawing.Color.SkyBlue;
            this.linestyleBox.Controls.Add(this.dotPnl);
            this.linestyleBox.Controls.Add(this.dashdotPnl);
            this.linestyleBox.Controls.Add(this.dashPnl);
            this.linestyleBox.Controls.Add(this.solidPnl);
            resources.ApplyResources(this.linestyleBox, "linestyleBox");
            this.linestyleBox.Name = "linestyleBox";
            this.linestyleBox.TabStop = false;
            // 
            // dotPnl
            // 
            resources.ApplyResources(this.dotPnl, "dotPnl");
            this.dotPnl.Name = "dotPnl";
            this.dotPnl.Click += new System.EventHandler(this.dotPnl_Click);
            // 
            // dashdotPnl
            // 
            resources.ApplyResources(this.dashdotPnl, "dashdotPnl");
            this.dashdotPnl.Name = "dashdotPnl";
            this.dashdotPnl.Click += new System.EventHandler(this.dashdotPnl_Click);
            // 
            // dashPnl
            // 
            resources.ApplyResources(this.dashPnl, "dashPnl");
            this.dashPnl.Name = "dashPnl";
            this.dashPnl.Click += new System.EventHandler(this.dashPnl_Click);
            // 
            // solidPnl
            // 
            this.solidPnl.BackColor = System.Drawing.Color.Red;
            resources.ApplyResources(this.solidPnl, "solidPnl");
            this.solidPnl.Name = "solidPnl";
            this.solidPnl.Click += new System.EventHandler(this.solidPnl_Click);
            // 
            // shapesBox
            // 
            this.shapesBox.BackColor = System.Drawing.Color.SkyBlue;
            this.shapesBox.Controls.Add(this.btnRectangle);
            this.shapesBox.Controls.Add(this.btnLine);
            this.shapesBox.Controls.Add(this.btnCircle);
            resources.ApplyResources(this.shapesBox, "shapesBox");
            this.shapesBox.Name = "shapesBox";
            this.shapesBox.TabStop = false;
            // 
            // pnlDrawing
            // 
            resources.ApplyResources(this.pnlDrawing, "pnlDrawing");
            this.pnlDrawing.BackColor = System.Drawing.Color.White;
            this.pnlDrawing.Name = "pnlDrawing";
            this.pnlDrawing.TabStop = false;
            this.pnlDrawing.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlDrawing_MouseDown);
            this.pnlDrawing.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnlDrawing_MouseMove);
            this.pnlDrawing.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pnlDrawing_MouseUp);
            // 
            // btnSepia
            // 
            resources.ApplyResources(this.btnSepia, "btnSepia");
            this.btnSepia.Name = "btnSepia";
            this.btnSepia.UseVisualStyleBackColor = true;
            this.btnSepia.Click += new System.EventHandler(this.btnSepia_Click);
            // 
            // Form1
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlDrawing);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.drawingBox.ResumeLayout(false);
            this.colorBox.ResumeLayout(false);
            this.lineBox.ResumeLayout(false);
            this.linestyleBox.ResumeLayout(false);
            this.shapesBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlDrawing)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem languageMenu;
        private System.Windows.Forms.ToolStripMenuItem aboutMenu;
        private System.Windows.Forms.ToolStripMenuItem newMenu;
        private System.Windows.Forms.ToolStripMenuItem saveAsMenu;
        private System.Windows.Forms.ToolStripMenuItem exitMenu;
        private System.Windows.Forms.ToolStripMenuItem openMenu;
        private System.Windows.Forms.ToolStripMenuItem polishMenu;
        private System.Windows.Forms.ToolStripMenuItem englishMenu;
        private System.Windows.Forms.ToolStripMenuItem deutschMenu;
        private System.Windows.Forms.ToolStripMenuItem editMenu;
        private System.Windows.Forms.ToolStripMenuItem undoMenu;
        private System.Windows.Forms.ToolStripMenuItem redoMenu;
        private System.Windows.Forms.Button btnColor;
        private System.Windows.Forms.Button btnLine;
        private System.Windows.Forms.Button btnRectangle;
        private System.Windows.Forms.Button btnCircle;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.ComboBox penWidth;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pnlDrawing;
        private System.Windows.Forms.GroupBox lineBox;
        private System.Windows.Forms.GroupBox linestyleBox;
        private System.Windows.Forms.GroupBox shapesBox;
        private System.Windows.Forms.GroupBox colorBox;
        private System.Windows.Forms.GroupBox drawingBox;
        private System.Windows.Forms.Button pencilBtn;
        private System.Windows.Forms.Panel solidPnl;
        private System.Windows.Forms.Panel dashPnl;
        private System.Windows.Forms.Panel dotPnl;
        private System.Windows.Forms.Panel dashdotPnl;
        private System.Windows.Forms.Button btnSepia;
    }
}

