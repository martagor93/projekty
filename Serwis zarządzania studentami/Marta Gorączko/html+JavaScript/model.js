"use strict";

var newStudent = {indeks: null, imie: null, nazwisko: null, dataUrodzenia: null};
var newCourse = {idPrzedmiotu: null, nazwa: null, prowadzacy: null};
var newGrade = {
    ocenaId: null, wartosc: null, data: null, przedmiot: { idPrzedmiotu: null, nazwa: null},
    student: { indeks: null, imie: null, nazwisko: null, dataUrodzenia: null }    
};
var actualCourseId = 0;

var view = {
    students: ko.observableArray(),
    courses: ko.observableArray(),
    grades: ko.observableArray(),
    filter: null,

    getStudent: function (student) {
        return student.imie() + " " + student.nazwisko() + " (" + student.indeks() + ")"
    },

    addStudent: function () {
        var path = "students/"
        var data = ko.toJSON(newStudent);
        create(path, data)
        getStudents();
    },

    updateStudent: function () {

        var student = this;
        var data1 = student.students;
        var i = 0;
        console.log(data1);
        for (i = 0; i < data1().length; i++) {
            $.ajax({
                url: "http://localhost:9998/students/" + data1()[i].indeks(),
                type: "PUT",
                data: ko.toJSON(data1()[i], function (key, value) { return key == "__ko_mapping__" ? undefined : value }),
                datatype: "json",
                processData: false,
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                }
            });
        }

    },

    showStudentGrades: function (singleStudent) {
        view.filter = "students/" + singleStudent.indeks() + "/grades"
        window.location.href = "#form_3"
    },

    deleteStudent: function (singleStudent) {
        $.ajax({
            url: "http://localhost:9998/students/" + singleStudent.indeks(),
            type: "DELETE",
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                ko.toJSON(singleStudent, function (key, value) { return key == "__ko_mapping__" ? undefined : value });
            },
            async: false
        })
        getStudents();
    },

    addCourse: function () {
        var path = "courses/"
        var data = ko.toJSON(newCourse)
        create(path, data)
        getCourses();
    },

    updateCourse: function () {
        var course = this;
        var data = course.courses;
        var i = 0;
        console.log(data)
        for (i = 0; i < data().length; i++) {
                $.ajax({
                    url: "http://localhost:9998/courses/" + data()[i].idPrzedmiotu(),
                    type: "PUT",
                    data: ko.toJSON(data()[i], function (key, value) { return key == "__ko_mapping__"?undefined:value }),
                    datatype: "json",
                    processData: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (result) {
                            
                    }
                });
            }
    },

    showCourseGrades: function (singleCourse) {
        actualCourseId = singleCourse.idPrzedmiotu();
        view.filter = "courses/" + singleCourse.idPrzedmiotu() + "/grades"
        window.location.href = "#form_3"
    },

    deleteCourse: function (singleCourse) {
        view.courses.remove(singleCourse);
        $.ajax({
            url: "http://localhost:9998/courses/" + singleCourse.idPrzedmiotu(),
            type: "DELETE",
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                ko.toJSON(singleCourse, function (key, value) { return key == "__ko_mapping__" ? undefined : value });
            },
            async: false
        })
        getCourses();
    },

    addGrade: function () {
        var path = "grades"
        if (actualCourseId != undefined && actualCourseId != 0) {
            newGrade.przedmiot = { idPrzedmiotu: actualCourseId };
            var data = ko.toJSON(newGrade)
            view.grades.push(newGrade)
            create(path, data)
            view.filter = "courses/" + newGrade.przedmiot.idPrzedmiotu + "/grades"
            getGradesByCourse()
        }
        else {
            var data = ko.toJSON(newGrade)
            view.grades.push(newGrade)
            create(path, data)
            getGrades()
        }
    },

    updateGrade: function (ocena) {
        var path = "grades/" + ocena.ocenaId()
        var data = ko.toJSON(ocena)
        update(path, data)
        getGrades();
    },

    deleteGrade: function (singleGrade) {
        $.ajax({
            url: "http://localhost:9998/grades/" + singleGrade.ocenaId(),
            type: "DELETE",
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                ko.toJSON(singleGrade, function (key, value) { return key == "__ko_mapping__" ? undefined : value });
            },
            async: false
        })
        getGrades();
    }
}

$(function () {
    ko.applyBindings(view)
});

$(window).on('hashchange', function () {
    if (location.hash == "#form_1")
        getStudents()
    if (location.hash == "#form_2")
        getCourses()
    if (location.hash == '#form_3') {
        getGrades()
    }
}).trigger('hashchange');

function getStudents() {
    $.ajax({
        url: "http://localhost:9998/students",
        type: "GET",
        beforeSend: function (data) {
            data.setRequestHeader('Accept', 'application/json');
        }, 
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            view.students.removeAll()
            var koData = ko.mapping.fromJS(data)
            for (var i in koData())
                view.students.push(koData()[i])
        },
        async: false
    });
}

function update(path, data) {
    $.ajax({
        url: "http://localhost:9998/" + path,
        type: "PUT",
        data: data,
        beforeSend: function (data) {
            data.setRequestHeader('Content-Type', 'application/json');
        },
        async: false
    });
}

function create(path, data) {
    $.ajax({
        url: "http://localhost:9998/" + path,
        type: "POST",
        data: data,
        beforeSend: function (data) {
            data.setRequestHeader('Content-Type', 'application/json');
        },
        async: false
    })
}

function getCourses() {
    $.ajax({
        url: "http://localhost:9998/courses",
        type: "GET",
        beforeSend: function (data) {
            data.setRequestHeader('Accept', 'application/json');
        },
        success: function (data) {
            view.courses.removeAll()
            var koData = ko.mapping.fromJS(data)
            for (var i in koData())
                view.courses.push(koData()[i])
        },
        async: false
    })
}

function getGrades() {

    var path = "grades"
    if (view.filter != null)
        path = view.filter

    $.ajax({
        url: "http://localhost:9998/" + path,
        type: "GET",
        beforeSend: function (data) {
            data.setRequestHeader('Accept', 'application/json');
        },
        success: function (data) {
            view.grades.removeAll()
            getStudents()
            getCourses()
            var koData = ko.mapping.fromJS(data)
            for (var i in koData()) {
                view.grades.push(koData()[i])
            }
            view.filter = null
        },
        async: false
    })
}

function getGradesByCourse() {
    var path = "grades"
    if (view.filter != null)
        path = view.filter

    $.ajax({
        url: "http://localhost:9998/" + path,
        type: "GET",
        beforeSend: function (data) {
            data.setRequestHeader('Accept', 'application/json');
        },
        success: function (data) {
            view.grades.removeAll()
            getStudents()
            getCourses()
            var koData = ko.mapping.fromJS(data)
            for (var i in koData()) {
                view.grades.push(koData()[i])
            }
            view.filter = null
        },
        async: false
    })
}