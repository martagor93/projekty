import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;

import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

/**
 * Created by Marta2 on 2017-09-02.
 */
@Path("grades")
public class OcenaControl {
    private Datastore store = Model.getInstance().store;
    private Query<Przedmiot> przedmiotQuery = store.createQuery(Przedmiot.class);
    private Query<Ocena> ocenaQuery = store.createQuery(Ocena.class);

    /*@GET
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public synchronized ArrayList<Ocena> getAllGrades(){
        ArrayList<Ocena> ocenaList = (ArrayList<Ocena>) ocenaQuery.asList();
        ArrayList<Przedmiot> przedmiotArrayList = (ArrayList<Przedmiot>) przedmiotQuery.asList();
        for (int i =0; i<przedmiotArrayList.size(); i++)
        {
            int j = i;
            String prowadzacy = "s";
            if (i<przedmiotArrayList.size()){
                Przedmiot przedmiot = przedmiotArrayList.get(i);
                j = przedmiot.getidPrzedmiotu();
                prowadzacy = przedmiot.getProwadzacy();
                ocenaQuery.field("przedmiot.idPrzedmiotu").equal(j);
                ArrayList<Ocena> ocenaList1 = (ArrayList<Ocena>) ocenaQuery.asList();
                for (int k = 0; k<ocenaList1.size(); k++)
                {
                    store.update(ocenaQuery.field("przedmiot.idPrzedmiotu").equal(j), store.createUpdateOperations(Ocena.class).set("przedmiot.prowadzacy",prowadzacy))
                    ;
                }
            }
        }
        ArrayList<Ocena> ocenaList2 = (ArrayList<Ocena>) store.createQuery(Ocena.class).asList();
        if (ocenaList2.size()>0){
            return ocenaList2;
        }
        else
            throw new NotFoundException();

    }*/

    @GET
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public synchronized ArrayList<Ocena> getAllGrades(){
        ArrayList<Ocena> ocenaList = (ArrayList<Ocena>) ocenaQuery.asList();
        if (ocenaList.size()>0){
            return ocenaList;
        }
        else
            throw new NotFoundException();
    }

    @PUT
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("{id}")
    public synchronized Response updateGrade(@PathParam("id") int ocenaId ,Ocena ocena){
        ocenaQuery.field("ocenaId").equal(ocenaId);
        Ocena ocena1 = ocenaQuery.get();
        if(ocena1 != null ){
            ocena1.setOcenaId(ocenaId);
            ocena1.setPrzedmiot(ocena.getPrzedmiot());
            ocena1.setStudent(ocena.getStudent());
            ocena1.setData(ocena.getData());
            ocena1.setWartosc(ocena.getWartosc());
            store.update(Model.getInstance().store.createQuery(Ocena.class).field("ocenaId").equal(ocena1.getOcenaId()), store.createUpdateOperations(Ocena.class).set("przedmiot",ocena1.getPrzedmiot())
                            .set("student",ocena1.getStudent())
                            .set("wartosc",ocena1.getWartosc())
                            .set("data",ocena1.getData()));
            return Response.ok(ocena).build();
        }
        else
            return Response.status(404).build();
    }


    @POST
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public synchronized Response addGradeToCourse(Ocena ocena) throws URISyntaxException {
        Ocena ocena1 = new Ocena();
        ocena1.setOcenaId(Model.getInstance().createOcenaId());
        ocena1.setPrzedmiot(ocena.getPrzedmiot());
        ocena1.setStudent(ocena.getStudent());
        ocena1.setData(ocena.getData());
        ocena1.setWartosc(ocena.getWartosc());
        //ocena1.setOcenaId(Model.getInstance().createOcenaId());
        store.save(ocena1);
        if (ocena1 != null){
            //UriBuilder builder = uriInfo.getAbsolutePathBuilder();
            return Response.created(new URI(String.format("/grades/%d", ocena1.getOcenaId()))).entity(ocena1).build();
        }
        else
            return Response.status(404).build();
    }


    @DELETE
    @Produces ({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("{ocenaId}")
    public synchronized Response deleteGrade(@PathParam("ocenaId") int singleGrade){
        ocenaQuery.field("ocenaId").equal(singleGrade);
        Ocena ocena1 = ocenaQuery.get();
        if(ocena1 != null) {
            store.delete(ocenaQuery);
            return Response.status(204).header("Ocena o identyfikatorze"+ singleGrade, "została usunięta").build();
        }else{
            return Response.status(404).build();
        }
    }
}
