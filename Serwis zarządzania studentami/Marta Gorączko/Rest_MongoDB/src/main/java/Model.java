import com.mongodb.MongoClient;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by student on 26.02.2017.
 */
public class Model {

    Datastore store;
    final Morphia m = new Morphia();
    private int indeks = 0;
    private int przedmiotId = 2;
    private int ocenaId = 4;


    private static Model instance = null;

    private Model(){
        store = m.createDatastore(new MongoClient(), "model3");
        store.ensureIndexes();
        data();
    }
    public static Model getInstance(){
        if(instance == null){
            instance = new Model();
            return instance;
        }else{
            return instance;
        }
    }

    public int createIndeks()
    {
        indeks++;
        return indeks;
    }
    public int createPrzedmiotId()
    {
        przedmiotId++;
        return przedmiotId;
    }
    public int createOcenaId()
    {
        ocenaId++;
        return ocenaId;
    }

    private void data(){

        if (store.getCount(Student.class) == 0) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date studentData = new Date();
            try {
                studentData = dateFormat.parse("1993-10-11");
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Student student = new Student(112824, "Marta", "Goraczko", studentData);
            store.save(student);


            Date studentData1 = new Date();
            try {
                studentData1 = dateFormat.parse("1993-05-20");
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Student student1 = new Student(123908, "Jan", "Kowalski", studentData1);
            store.save(student1);


            Przedmiot przedmiot3 = new Przedmiot(1, "Bazy danych", "Nowak");
            store.save(przedmiot3);

            Przedmiot przedmiot4 = new Przedmiot(2, "ZPO", "Jankowski");
            store.save(przedmiot4);


            Ocena ocena = new Ocena(1, 3.0, new Date(), student, przedmiot3);
            store.save(ocena);

            Ocena ocena1 = new Ocena(2, 3.5, new Date(), student, przedmiot4);
            store.save(ocena1);


            Ocena ocena3 = new Ocena(3, 4.0, new Date(), student1, przedmiot3);
            store.save(ocena3);

            Ocena ocena4 = new Ocena(4, 4.5, new Date(), student1, przedmiot4);
            store.save(ocena4);


        }
    }

}
