import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

/**
 * Created by student on 26.02.2017.
 */
@Path("courses")
public class PrzedmiotControl {

    private Datastore store = Model.getInstance().store;
    private Query<Przedmiot> przedmiotQuery = store.createQuery(Przedmiot.class);
    private Query<Ocena> ocenaQuery = store.createQuery(Ocena.class);


    @GET
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public synchronized ArrayList<Przedmiot> getCourses(@QueryParam("prowadzacy") String prowadzacy){
        if (prowadzacy != null){
            przedmiotQuery.field("prowadzacy").containsIgnoreCase(prowadzacy);
        }
        ArrayList<Przedmiot> coursesList = (ArrayList<Przedmiot>) przedmiotQuery.asList();
        if (coursesList.size() > 0){
            return coursesList;
        }
        else
            throw new NotFoundException();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("{idPrzedmiotu}")
    public synchronized Przedmiot getCourse(@PathParam("idPrzedmiotu") int identity){
        przedmiotQuery.field("idPrzedmiotu").equal(identity);
        Przedmiot przedmiot = przedmiotQuery.get();
        if (przedmiot != null){
            return przedmiot;
        }
        else
            throw new NotFoundException();
    }


    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("{idPrzedmiotu}/grades")
    public synchronized ArrayList<Ocena> getCourseGrades(@PathParam("idPrzedmiotu") int idPrzedmiotu){
        przedmiotQuery.field("idPrzedmiotu").equal(idPrzedmiotu);
        Przedmiot przedmiot1 = przedmiotQuery.get();
        ocenaQuery.field("przedmiot.idPrzedmiotu").equal(przedmiot1.getidPrzedmiotu());
        ArrayList<Ocena> ocenaArrayList = (ArrayList<Ocena>) ocenaQuery.asList();
        if (ocenaArrayList.size() > 0){
            return ocenaArrayList;
        }
        else
            throw new
                    NotFoundException();

    }

    @POST
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public synchronized Response addCourse(Przedmiot przedmiot) throws URISyntaxException{
        Przedmiot przedmiot1 = new Przedmiot();
        przedmiot1.setidPrzedmiotu(Model.getInstance().createPrzedmiotId());
        przedmiot1.setNazwa(przedmiot.getNazwa());
        przedmiot1.setProwadzacy(przedmiot.getProwadzacy());
        store.save(przedmiot1);
        if(przedmiot != null){

            return Response.created(new URI(String.format("/courses/%d", przedmiot1.getidPrzedmiotu()))).entity(przedmiot1).build();
        }
        else
            return Response.status(404).build();
    }

    @PUT
    @Produces ({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("{idPrzedmiotu}")
    public synchronized Response updateCourse(@PathParam("idPrzedmiotu") int idPrzedmiotu, Przedmiot przedmiot){
        przedmiotQuery.field("idPrzedmiotu").equal(idPrzedmiotu);
        Przedmiot przedmiot1 = przedmiotQuery.get();
        if (przedmiot1 != null){
            przedmiot1.setidPrzedmiotu(idPrzedmiotu);
            przedmiot1.setProwadzacy(przedmiot.getProwadzacy());
            przedmiot1.setNazwa(przedmiot.getNazwa());
            store.update(przedmiotQuery.field("idPrzedmiotu").equal(przedmiot1.getidPrzedmiotu()),
                    store.createUpdateOperations(Przedmiot.class).set("nazwa", przedmiot.getNazwa()).set("prowadzacy", przedmiot.getProwadzacy()));
            return Response.ok(przedmiot).build();
        }
        else
            return Response.status(404).build();
    }


    @DELETE
    @Produces ({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("{idPrzedmiotu}")
    public synchronized Response removeCourse(@PathParam("idPrzedmiotu") int idPrzedmiotu) {
        przedmiotQuery.field("idPrzedmiotu").equal(idPrzedmiotu);
        Przedmiot przedmiot = przedmiotQuery.get();
        if(przedmiot != null) {
            store.delete(przedmiotQuery);
            ocenaQuery.field("przedmiot.idPrzedmiotu").equal(przedmiot.getidPrzedmiotu());
            store.delete(ocenaQuery);
            return Response.status(204).build();
        }
        else
            return Response.status(404).build();
    }
}
