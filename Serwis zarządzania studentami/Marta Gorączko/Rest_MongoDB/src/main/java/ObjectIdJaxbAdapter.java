import org.bson.types.ObjectId;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Created by Marta2 on 2017-05-23.
 */
public class ObjectIdJaxbAdapter extends XmlAdapter<String, ObjectId> {
    @Override
    public ObjectId unmarshal(String objectId) throws Exception {
        if (objectId == null)
            return null;
        return new ObjectId(objectId);
    }

    @Override
    public String marshal(ObjectId objectId) throws Exception {
        if (objectId == null)
            return null;
        return objectId.toString();
    }
}
