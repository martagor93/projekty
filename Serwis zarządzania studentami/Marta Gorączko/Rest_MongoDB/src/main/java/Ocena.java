import com.fasterxml.jackson.annotation.JsonFormat;
import jdk.nashorn.internal.ir.annotations.Reference;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Property;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Date;

/**
 * Created by student on 26.02.2017.
 */
@Entity("grades")
@XmlRootElement
public class Ocena {

    @Id()
    @XmlTransient
    private ObjectId oId;
    @Property("ocenaId")
    @Indexed(unique = true)
    private int ocenaId;
    @Property("wartosc")
    private double wartosc;
    @Property("data")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="CET")
    private Date data;
    @Reference
    private Student student;
    @Reference
    private Przedmiot przedmiot;


    public Ocena(){

    }

    public Ocena(int ocenaId, double wartosc, Date data, Student student, Przedmiot przedmiot){
        this.ocenaId = ocenaId;
        this.wartosc =wartosc;
        this.data = data;
        this.student = new Student(student.getIndeks(), student.getImie(), student.getNazwisko(), student.getDataUrodzenia());
        this.przedmiot = new Przedmiot(przedmiot.getidPrzedmiotu(), przedmiot.getNazwa(), przedmiot.getProwadzacy());
    }
    @XmlTransient
    public ObjectId getoId() {
        return oId;
    }

    public void setoId(ObjectId oId) {
        this.oId = oId;
    }

    public int getOcenaId() {return ocenaId;}

    public void setOcenaId(int ocenaId) {this.ocenaId = ocenaId;}

    public double getWartosc() {
        return wartosc;
    }

    public void setWartosc(double wartosc) {
        this.wartosc = wartosc;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Student getStudent() {return student;}
    public void setStudent(Student student) {this.student = student;}

    public Przedmiot getPrzedmiot() {return przedmiot;}
    public void setPrzedmiot(Przedmiot przedmiot) {this.przedmiot = przedmiot;}

}
