import com.sun.javafx.sg.prism.NGShape;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.ws.rs.DELETE;

/**
 * Created by student on 26.02.2017.
 */

@Path("students")
public class StudentControl {

    private Datastore store = Model.getInstance().store;
    private Query<Student> studentQuery = store.createQuery(Student.class);
    private Query<Ocena> ocenaQuery = store.createQuery(Ocena.class);

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("{indeks}")
    public synchronized Response getStudent(@PathParam("indeks") int indeks){
        studentQuery.field("indeks").equal(indeks);
        Student student = studentQuery.get();
        if(student != null){
            return Response.ok(student).build();
        }else{
            return Response.status(404).build();
        }
    }

    @GET
    @Path("{indeks}/grades")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public synchronized Response getStudentGrades(@PathParam("indeks") int indeks, @QueryParam("przedmiotId") int przedmiotId, @QueryParam("greater") Double greater,@QueryParam("less") Double gradeValue) {
        studentQuery.field("indeks").equal(indeks);
        Student st = studentQuery.get();
        ocenaQuery.field("student.indeks").equal(st.getIndeks());
        if(przedmiotId > 0) {
            ocenaQuery.field("przedmiot.IdPrzedmiotu").equal(przedmiotId);
        }

        if(greater != null) {
            ocenaQuery.field("wartosc").greaterThan(greater);
        }

        if(gradeValue != null){
            if(greater == null){
                ocenaQuery.field("wartosc").lessThan(gradeValue);
            }else if(greater != null && greater<gradeValue){
                ocenaQuery.field("wartosc").lessThan(gradeValue);
            }

        }

        ArrayList<Ocena> ocenaArrayList = (ArrayList<Ocena>) ocenaQuery.asList();

        if(ocenaArrayList.size() > 0) {
            GenericEntity<ArrayList<Ocena>> grades = new GenericEntity<ArrayList<Ocena>>(ocenaArrayList){};
            return Response.ok(grades).build();
        }else{
            return Response.status(404).build();
        }
    }

    @GET
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public synchronized Response getAllStudentsByName(@QueryParam("imie") String imie, @QueryParam("nazwisko") String nazwisko, @QueryParam("birthDate") String birthDate, @QueryParam("dateBefore") String dateBefore, @QueryParam("dateAfter") String dateAfter) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("CET"));
        Date birthDate1 = null;
        if (birthDate != null) {
            try {
                birthDate1 = simpleDateFormat.parse(birthDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        Date birthDate2 = null;
        if (dateBefore != null) {
            try {
                birthDate2 = simpleDateFormat.parse(dateBefore);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        Date birthDate3 = null;
        if (dateAfter != null){
            try {
                birthDate3 = simpleDateFormat.parse(dateAfter);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if (birthDate2 != null){
            studentQuery.field("dataUrodzenia").lessThan(birthDate2);
        }
        if (birthDate3 != null){
            studentQuery.field("dataUrodzenia").greaterThan(birthDate3);
        }
        if (birthDate1 != null){
            studentQuery.field("dataUrodzenia").equal(birthDate1);
        }
        if(imie != null) {
            studentQuery.field("imie").containsIgnoreCase(imie);
        }

        if(nazwisko != null) {
            studentQuery.field("nazwisko").containsIgnoreCase(nazwisko);
        }

        ArrayList<Student> students = (ArrayList<Student>) studentQuery.asList();
        if (students.size() > 0) {
            return Response.ok(new GenericEntity<ArrayList<Student>>(students) {
            }).build();
        } else {
            return Response.status(404).build();
        }
    }


    @POST
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public synchronized Response addStudentToList(Student student1) throws URISyntaxException{
        student1.setIndeks(Model.getInstance().createIndeks());
        store.save(student1);
        if(student1 != null){
            //UriBuilder builder = uriInfo.getAbsolutePathBuilder();
            return Response.created(new URI(String.format("/students/%d", student1.getIndeks()))).entity(student1).build();
        }
        else
            return Response.status(404).build();
    }

    @PUT
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("{indeks}")
    public synchronized Response updateStudent(@PathParam("indeks") int indeks,Student student){
        studentQuery.field("indeks").equal(indeks);
        Student student1 = studentQuery.get();
        if (student1 != null){
            student1.setIndeks(indeks);
            student1.setDataUrodzenia(student.getDataUrodzenia());
            student1.setImie(student.getImie());
            student1.setNazwisko(student.getNazwisko());
            store.update(store.createQuery(Student.class).field("indeks").equal(student1.getIndeks()),
                    store.createUpdateOperations(Student.class).set("imie",student1.getImie())
                            .set("nazwisko",student1.getNazwisko())
                            .set("dataUrodzenia",student1.getDataUrodzenia()));
            return Response.ok(student1).build();
        }
        else{
            return Response.status(404).build();
        }
    }

    @DELETE
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("{indeks}")
    public synchronized Response deleteStudent(@PathParam("indeks") int id){
        studentQuery.field("indeks").equal(id);
        Student st = studentQuery.get();
        if(st != null) {
            store.delete(st);
            ocenaQuery.field("student.indeks").equal(st.getIndeks());
            store.delete(ocenaQuery);
            return Response.status(204).build();
        }else{
            return Response.status(404).build();
        }
    }
}

