import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Property;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Created by student on 26.02.2017.
 */
@Entity("courses")
@XmlRootElement
public class Przedmiot {

    @Id()
    @XmlTransient
    private ObjectId oId;
    @Property("idPrzedmiotu")
    @Indexed(unique = true)
    private int idPrzedmiotu;
    @Property("nazwa")
    private String nazwa;
    @Property("prowadzacy")
    private String prowadzacy;


    public Przedmiot(){

    }
    public Przedmiot(int idPrzedmiotu, String nazwa, String prowadzacy){
        this.idPrzedmiotu = idPrzedmiotu;
        this.nazwa = nazwa;
        this.prowadzacy = prowadzacy;
    }
    @XmlTransient
    public ObjectId getoId() {
        return oId;
    }

    public void setoId(ObjectId oId) {
        this.oId = oId;
    }
    @XmlElement
    public int getidPrzedmiotu() {return idPrzedmiotu;}

    public void setidPrzedmiotu(int idPrzedmiotu) {this.idPrzedmiotu = idPrzedmiotu;}
    @XmlElement
    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }
    @XmlElement
    public String getProwadzacy() {
        return prowadzacy;
    }

    public void setProwadzacy(String prowadzacy) {
        this.prowadzacy = prowadzacy;
    }

}
