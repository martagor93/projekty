﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Tetrahedron : MonoBehaviour
{
    static Matrix3 matrix;
    static bool choosenIndex;
    private GameObject sphere;
    public static List<GameObject> spheres = new List<GameObject>();
    public static bool stopAnimation = false;
    public static string rotationAxis;
    public static bool scale = false;

    public void Start()
    {
        choosenIndex = false;
        rotationAxis = "x";
        foreach (var item in matrix.Pairs.Values)
        {
            sphere = new GameObject("sphere");
            sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphere.transform.localScale = new Vector3(3.5f, 3.5f, 3.5f);
            sphere.transform.parent = this.gameObject.transform;
            sphere.transform.position = item;
            MeshRenderer sphereRenderer = sphere.GetComponent<MeshRenderer>();
            sphereRenderer.receiveShadows = false;
            Material material = new Material(Shader.Find("Standard"));
            material.color = Color.blue;
            sphereRenderer.material = material;
            spheres.Add(sphere);
        }

        if (scale == true)
        {
            var camera = Camera.main;
            camera.transform.localPosition = new Vector3(0, -1, -350);
        }

        SetVertexPositions(GetComponentsInChildren<TextMesh>());        
    }

    void DoUpdateOnScene()
    {
        if (choosenIndex == true)
        {
            foreach (var item in Equations.Results)
            {
                var test = spheres.Find(x => x.transform.localPosition == item.Vector3);
                MeshRenderer cubeRenderer = test.GetComponent<MeshRenderer>();
                Material material = new Material(Shader.Find("Standard"));
                material.color = item.Color;
                cubeRenderer.material = material;
            }
            choosenIndex = false;
        }

        if (!stopAnimation)
        {
            switch (rotationAxis)
            {
                case "x":
                    transform.Rotate(new Vector3(Time.deltaTime * 20, 0, 0));
                    break;
                case "y":
                    transform.Rotate(new Vector3(0, Time.deltaTime * 20, 0));
                    break;
                case "z":
                    transform.Rotate(new Vector3(0, 0, Time.deltaTime * 20));
                    break;
                case "xy":
                    transform.Rotate(new Vector3(Time.deltaTime * 20, Time.deltaTime * 20, 0));
                    break;
                case "xz":
                    transform.Rotate(new Vector3(Time.deltaTime * 20, 0, Time.deltaTime * 20));
                    break;
                case "yz":
                    transform.Rotate(new Vector3(0, Time.deltaTime * 20, Time.deltaTime * 20));
                    break;
                case "xyz":
                    transform.Rotate(new Vector3(Time.deltaTime * 20, Time.deltaTime * 20, Time.deltaTime * 20));
                    break;
            }
        }
    }

    public void Update()
    {        
        if (choosenIndex == true)
        {            
            foreach (var item in Equations.Results)
            {
                var test = spheres.Find(x => x.transform.localPosition == item.Vector3);
                MeshRenderer cubeRenderer = test.GetComponent<MeshRenderer>();
                Material material = new Material(Shader.Find("Standard"));
                material.color = item.Color;
                cubeRenderer.material = material;
            }     

            choosenIndex = false;
        }       

        if (!stopAnimation)
        {
            switch (rotationAxis)
            {
                case "x":
                    transform.Rotate(new Vector3(Time.deltaTime * 20, 0, 0));
                    break;
                case "y":
                    transform.Rotate(new Vector3(0, Time.deltaTime * 20, 0));
                    break;
                case "z":
                    transform.Rotate(new Vector3(0, 0, Time.deltaTime * 20));
                    break;
                case "xy":
                    transform.Rotate(new Vector3(Time.deltaTime * 20, Time.deltaTime * 20, 0));
                    break;
                case "xz":
                    transform.Rotate(new Vector3(Time.deltaTime * 20, 0, Time.deltaTime * 20));
                    break;
                case "yz":
                    transform.Rotate(new Vector3(0, Time.deltaTime * 20, Time.deltaTime * 20));
                    break;
                case "xyz":
                    transform.Rotate(new Vector3(Time.deltaTime * 20, Time.deltaTime * 20, Time.deltaTime * 20));
                    break;
            }
        }

        if (Input.GetKey("escape"))
        {            
                UnityEngine.Application.Quit();
        }
    }


    public void SetVertexPositions(TextMesh[] list)
    {
        for (int i = 0; i < list.Length; i++)
        {
            switch (list[i].text)
            {
                case "a":
                    list[i].gameObject.transform.localPosition = new Vector3(matrix.VectorSum, matrix.VectorSum + 5, matrix.VectorSum);
                    break;
                case "b":
                    list[i].gameObject.transform.localPosition = new Vector3(-matrix.VectorSum, matrix.VectorSum + 5, -matrix.VectorSum);
                    break;
                case "c":
                    list[i].gameObject.transform.localPosition = new Vector3(-matrix.VectorSum, -matrix.VectorSum, matrix.VectorSum);
                    break;
                case "d":
                    list[i].gameObject.transform.localPosition = new Vector3(matrix.VectorSum, -matrix.VectorSum + 5, -matrix.VectorSum);
                    break;
            }
        }
    }

    public static void DrawGizmosBasedOnMeasures(bool set)
    {
        choosenIndex = set;        
    }

    public static void ResetScene()
    {
        choosenIndex = false;
        for (int i = 0; i < spheres.Count; i++)
        {
            spheres[i].SetActive(true);
        }
    }

    public static Matrix3 Matrix3
    {
        get { return matrix; }
        set { matrix = value; }
    }
}
