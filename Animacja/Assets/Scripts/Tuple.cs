﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tuple  {
    
    private Vector4 vector;
    private Vector3 vector3;
    private double value;
    private Color color;

    public Tuple(Vector4 vector, Vector3 vector3, double value)
    {
        this.vector = vector;
        this.vector3 = vector3;
        this.value = value;
    }

    public Tuple(Vector4 vector, double value)
    {
        this.vector = vector;
        this.value = value;        
    }

    public Tuple()
    {

    }

    public Vector4 Vector
    {
        get { return vector; }
    }

    public double Value
    {
        get { return value; }
    }

    public Color Color
    {
        get { return color; }
        set { color = value; }
    }

    public Vector3 Vector3
    {
        get { return vector3; }
        set { vector3 = value; }
    }
}
