﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Matrix4
{

    private List<Vector4> set = new List<Vector4>();

    public Matrix4()
    {
        set = GiveMatrix4();
    }

    public Matrix4(int vectorSum)
    {
        set = GiveMatrix4(vectorSum);
    }

    public List<Vector4> GiveMatrix4()
    {
        List<Vector4> set1 = new List<Vector4>();        

        for (int i = 0; i <= 32; i++)
        {
            for (int j = 0; j <= 32; j++)
            {
                for (int k = 0; k <= 32; k++)
                {
                    for (int l = 0; l <= 32; l++)
                    {
                        Vector4 vector = new Vector4((float)i, (float)j, (float)k, (float)l);

                        if (AddValuesFromVector4(vector) == 32)
                        {
                            set1.Add(vector);
                            continue;
                        }
                    }
                }
            }
        }

        return set1;
    }

    public List<Vector4> GiveMatrix4(int vectorSum)
    {
        List<Vector4> set1 = new List<Vector4>();

        for (int i = 0; i <= vectorSum; i++)
        {
            for (int j = 0; j <= vectorSum; j++)
            {
                for (int k = 0; k <= vectorSum; k++)
                {
                    for (int l = 0; l <= vectorSum; l++)
                    {
                        Vector4 vector = new Vector4((float)i, (float)j, (float)k, (float)l);

                        if (AddValuesFromVector4(vector) == vectorSum)
                        {
                            set1.Add(vector);
                            continue;
                        }
                    }
                }
            }
        }

        return set1;
    }

    public int AddValuesFromVector4(Vector4 vector)
    {
        int sum = 0;

        sum = (int)(vector.x + vector.y + vector.z + vector.w);

        return sum;
    }


    public List<Vector4> Matrix4Set
    {
        get { return set; }
    }
}
