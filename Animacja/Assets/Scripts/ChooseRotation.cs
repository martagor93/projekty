﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChooseRotation : MonoBehaviour {

    public Dropdown list;
	
	void Start () {

        List<string> rotationList = new List<string>()
        {
            "x",
            "y",
            "z",
            "xy",
            "xz",
            "yz",
            "xyz"
        };
        list.AddOptions(rotationList);
	}
	
	public void Dropdown_IndexChanged(int index)
    {
        switch(index)
        {
            case 0:
                Tetrahedron.rotationAxis = "x";
                break;
            case 1:
                Tetrahedron.rotationAxis = "y";
                break;
            case 2:
                Tetrahedron.rotationAxis = "z";
                break;
            case 3:
                Tetrahedron.rotationAxis = "xy";
                break;
            case 4:
                Tetrahedron.rotationAxis = "xz";
                break;
            case 5:
                Tetrahedron.rotationAxis = "yz";
                break;
            case 6:
                Tetrahedron.rotationAxis = "xyz";
                break;
        }
    }
}
