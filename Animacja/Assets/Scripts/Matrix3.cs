﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Matrix3
{
    private Matrix4 matrix4;
    private Dictionary<Vector4, Vector3> pairs = new Dictionary<Vector4, Vector3>();
    private int vectorSum;

    public Matrix3()
    {
        matrix4 = new Matrix4();
        pairs = CreateMatrix3(matrix4.Matrix4Set);        
    }

    public Matrix3(int vectorSum)
    {
        this.vectorSum = vectorSum;
        matrix4 = new Matrix4(vectorSum);
        pairs = CreateMatrix3(matrix4.Matrix4Set);
    }

    public int VectorSum
    {
        get { return vectorSum; }
    }

    public Dictionary<Vector4, Vector3> CreateMatrix3(List<Vector4> set)
    {        
        Vector3 vector;
        Dictionary<Vector4, Vector3> pair = new Dictionary<Vector4, Vector3>();

        foreach (var item in set)
        {
            vector = new Vector3(item.x - item.y - item.z + item.w, item.x + item.y - item.z - item.w, item.x - item.y + item.z - item.w);            
            pair.Add(item, vector);
        }

        return pair;
    }

    public int Sum(Vector3 vector)
    {
        int suma = 0;

        suma = (int)(vector.x + vector.y + vector.z);

        return suma;
    }

    public Dictionary<Vector4, Vector3> Pairs
    {
        get { return pairs; }
    }

    public Matrix4 Matrix4
    {
        get { return matrix4; }
    }
}
