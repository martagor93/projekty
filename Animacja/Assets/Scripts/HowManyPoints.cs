﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HowManyPoints : MonoBehaviour {

    public Dropdown list;
    static int vectorSum;

    public static int VectorSum
    {
        get { return vectorSum; }
        private set { vectorSum = value; }
    }

	void Start () {

        List<string> pointsList = new List<string>()
        {
            "969 (suma współrzędnych wektora równa 16)",
            "6545 (suma współrzędnych wektora równa 32)",
            "47905 (suma współrzędnych wektora równa 64)"
        };
        vectorSum = 16;
        list.AddOptions(pointsList);
	}

    void Update()
    {
        if (Input.GetKey("escape"))
        {            
           UnityEngine.Application.Quit();
        }
    }

    public void Dropdown_IndexChanged(int index)
    {
        switch(index)
        {
            case 0:
                vectorSum = 16;
                break;
            case 1:
                vectorSum = 32;
                break;
            case 2:
                vectorSum = 64;
                break;
        }
    }
}
