﻿using System;
using System.Collections.Generic;
using UnityEngine;
using NCalc;
using System.Collections;

public class Equations
{

    private static List<Tuple> results;
    private static Color[] colorsArray = new Color[4] { Color.blue, Color.green, Color.yellow, Color.red};
    private int index; 

    public class Row
    {
        private Color oldColor;
        private Color newColor;
        private float value;

        public Row()
        {

        }

        public Row(Color oldColor, Color newColor, float value)
        {
            this.oldColor = oldColor;
            this.newColor = newColor;
            this.value = value;
        }

        public Color GetOldColor
        {
            get { return oldColor; }
        }

        public Color GetNewColor
        {
            get { return newColor; }
        }

        public float GetValue
        {
            get { return value; }
        }
    }    

    public int Index
    {
        get { return index; }
        set { index = value; }
    }

    public static List<Tuple> Results
    {
        get { return results; }
        set { results = value; }
    }

    public static void CalculateAccuracy(Dictionary<Vector4, Vector3> set)
    {        
        Tuple tuple = new Tuple();
        results = new List<Tuple>();        

        foreach (var item in set.Keys)
        {                        
            double accuracy = (item.x + item.w) / (item.x + item.y + item.z + item.w);
            tuple = new Tuple(item, set[item], accuracy);
            if (double.IsNaN(accuracy))
                tuple.Color = Color.magenta;
            else if (double.IsPositiveInfinity(accuracy))
                tuple.Color = Color.red;
            else
            {
                tuple.Color = Color.Lerp(GiveColor(accuracy).GetOldColor, GiveColor(accuracy).GetNewColor, GiveColor(accuracy).GetValue);
            }             
            results.Add(tuple);                                                         
        }
        ColorScale.callFunctionOnce = false;
    }

    public static void CalculateAreaUnderLift(Dictionary<Vector4, Vector3> set)
    {
        results = new List<Tuple>();        
        Tuple tuple = new Tuple();

        foreach (var item in set.Keys)
        {
            double B = (item.x / (item.x + item.z) + item.w / (item.y + item.w)) / 2;
            double P = (item.x + item.z) / (item.x + item.y + item.z + item.w);
            double areaUnderLift = P / 2 + (1 - P) * B;            
            tuple = new Tuple(item, set[item], areaUnderLift);
            if (double.IsNaN(areaUnderLift))
                tuple.Color = Color.magenta;
            else if (double.IsPositiveInfinity(areaUnderLift))
                tuple.Color = Color.red;
            else
            {
                tuple.Color = Color.Lerp(GiveColor(areaUnderLift).GetOldColor, GiveColor(areaUnderLift).GetNewColor, GiveColor(areaUnderLift).GetValue);
            }                
            results.Add(tuple);
        }
        ColorScale.callFunctionOnce = false;
    }

    public static void CalculateBalancedAccuracy(Dictionary<Vector4, Vector3> set)
    {
        results = new List<Tuple>();        
        Tuple tuple = new Tuple();

        foreach (var item in set.Keys)
        {            
            double balancedAccuracy = 0.5f * (item.x / (item.x + item.z) + item.w / (item.y + item.w));
            tuple = new Tuple(item, set[item], balancedAccuracy);
            if (double.IsNaN(balancedAccuracy))
                tuple.Color = Color.magenta;
            else if (double.IsPositiveInfinity(balancedAccuracy))
                tuple.Color = Color.red;
            else
                tuple.Color = Color.Lerp(GiveColor(balancedAccuracy).GetOldColor, GiveColor(balancedAccuracy).GetNewColor, GiveColor(balancedAccuracy).GetValue);                   
            results.Add(tuple);
        }
        ColorScale.callFunctionOnce = false;
    }

    public static void CalculateDiagnosticOddsRatio(Dictionary<Vector4, Vector3> set)
    {
        results = new List<Tuple>();        
        Tuple tuple = new Tuple();

        foreach (var item in set.Keys)
        {            
            double diagnosticOddsRatio = item.x * item.w / item.z * item.y;
            tuple = new Tuple(item, set[item], diagnosticOddsRatio);
            if (double.IsNaN(diagnosticOddsRatio))
                tuple.Color = Color.magenta;
            else if (double.IsPositiveInfinity(diagnosticOddsRatio))
                tuple.Color = Color.red;
            else if (diagnosticOddsRatio == 0)
                tuple.Color = Color.blue;
            else if (diagnosticOddsRatio > 0 && diagnosticOddsRatio <= 1)
                tuple.Color = Color.Lerp(Color.blue, Color.green, (float)diagnosticOddsRatio);
            else if (diagnosticOddsRatio > 1 && diagnosticOddsRatio < double.PositiveInfinity)
                tuple.Color = Color.yellow;                
            results.Add(tuple);
        }
        ColorScale.callFunctionOnce = false;
    }

    public static void CalculateF1Score(Dictionary<Vector4, Vector3> set)
    {
        results = new List<Tuple>();        
        Tuple tuple = new Tuple();

        foreach (var item in set.Keys)
        {            
            double f1Score = 2 * (item.x / (item.x + item.z) * item.x / (item.x + item.y)) / (item.x / (item.x + item.y) + item.x / (item.x + item.z));
            tuple = new Tuple(item, set[item], f1Score);
            if (double.IsNaN(f1Score))
                tuple.Color = Color.magenta;
            else if (double.IsPositiveInfinity(f1Score))
                tuple.Color = Color.red;
            else
                tuple.Color = Color.Lerp(GiveColor(f1Score).GetOldColor, GiveColor(f1Score).GetNewColor, GiveColor(f1Score).GetValue);
            results.Add(tuple);
        }
        ColorScale.callFunctionOnce = false;
    }

    public static void CalculateFalseNegativeRate(Dictionary<Vector4, Vector3> set)
    {
        results = new List<Tuple>();        
        Tuple tuple = new Tuple();

        foreach (var item in set.Keys)
        {
            
            double falseNegativeRate = item.z / (item.z + item.x);
            tuple = new Tuple(item, set[item], falseNegativeRate);
            if (double.IsNaN(falseNegativeRate))
                tuple.Color = Color.magenta;
            else if (double.IsPositiveInfinity(falseNegativeRate))
                tuple.Color = Color.red;
            else
                tuple.Color = Color.Lerp(GiveColor(falseNegativeRate).GetOldColor, GiveColor(falseNegativeRate).GetNewColor, GiveColor(falseNegativeRate).GetValue);
            results.Add(tuple);
        }
        ColorScale.callFunctionOnce = false;
    }

    public static void CalculateFalsePositiveRate(Dictionary<Vector4, Vector3> set)
    {
        results = new List<Tuple>();        
        Tuple tuple = new Tuple();

        foreach (var item in set.Keys)
        {            
            double falsePositiveRate = item.y / (item.y + item.w);
            tuple = new Tuple(item, set[item], falsePositiveRate);
            if (double.IsNaN(falsePositiveRate))
                tuple.Color = Color.magenta;
            else if (double.IsPositiveInfinity(falsePositiveRate))
                tuple.Color = Color.red;
            else
                tuple.Color = Color.Lerp(GiveColor(falsePositiveRate).GetOldColor, GiveColor(falsePositiveRate).GetNewColor, GiveColor(falsePositiveRate).GetValue);
            results.Add(tuple);
        }
        ColorScale.callFunctionOnce = false;
    }

    public static void CalculateGMean(Dictionary<Vector4, Vector3> set)
    {
        results = new List<Tuple>();        
        Tuple tuple = new Tuple();

        foreach (var item in set.Keys)
        {            
            double gMean = Math.Sqrt((item.x / (item.x + item.z)) * (item.w / (item.y + item.w)));
            tuple = new Tuple(item, set[item], gMean);
            if (double.IsNaN(gMean))
                tuple.Color = Color.magenta;
            else if (double.IsPositiveInfinity(gMean))
                tuple.Color = Color.red;
            else
                tuple.Color = Color.Lerp(GiveColor(gMean).GetOldColor, GiveColor(gMean).GetNewColor, GiveColor(gMean).GetValue);
            results.Add(tuple);
        }
        ColorScale.callFunctionOnce = false;
    }

    public static void CalculateJaccardCoefficient(Dictionary<Vector4, Vector3> set)
    {
        results = new List<Tuple>();        
        Tuple tuple = new Tuple();

        foreach (var item in set.Keys)
        {            
            double jaccardCoefficient = item.x / (item.x + item.y + item.z);
            tuple = new Tuple(item, set[item], jaccardCoefficient);
            if (double.IsNaN(jaccardCoefficient))
                tuple.Color = Color.magenta;
            else if (double.IsPositiveInfinity(jaccardCoefficient))
                tuple.Color = Color.red;
            else
                tuple.Color = Color.Lerp(GiveColor(jaccardCoefficient).GetOldColor, GiveColor(jaccardCoefficient).GetNewColor, GiveColor(jaccardCoefficient).GetValue);
            results.Add(tuple);
        }
        ColorScale.callFunctionOnce = false;
    }

    public static void CalculateKappa(Dictionary<Vector4, Vector3> set)
    {
        results = new List<Tuple>();        
        Tuple tuple = new Tuple();

        foreach (var item in set.Keys)
        {           
            double accuracy = (item.x + item.w) / (item.x + item.y + item.z + item.w);
            double oneN = 1 / (item.x + item.y + item.z + item.w); ;
            double p = (item.x + item.z) * (item.x + item.y);
            double pN = p / (item.x + item.y + item.z + item.w);
            double n = (item.y + item.w) * (item.z + item.w);
            double nN = n / (item.x + item.y + item.z + item.w);
            double counter = accuracy - oneN * (pN + nN);
            double denominator = 1 - oneN * (pN + nN);
            double kappa = counter / denominator;
     
            tuple = new Tuple(item, set[item], kappa);
            if (double.IsNaN(kappa))
                tuple.Color = Color.magenta;
            else if (double.IsPositiveInfinity(kappa) || kappa == 1)
                tuple.Color = Color.red;
            else if (kappa < 0.2)
                tuple.Color = Color.Lerp(Color.blue, Color.green, (float)kappa * 20);
            else if (kappa > 0.5)
                tuple.Color = Color.Lerp(Color.yellow, Color.yellow, (float)kappa / 1f);
            else
                tuple.Color = Color.yellow;            
            results.Add(tuple);
        }
        ColorScale.callFunctionOnce = false;
    }

    public static void CalculateLogOddsRatio(Dictionary<Vector4, Vector3> set)
    {
        results = new List<Tuple>();        
        Tuple tuple = new Tuple();

        foreach (var item in set.Keys)
        {            
            double logOddsRatio = Math.Log((item.x * item.w) / (item.y * item.z));
            tuple = new Tuple(item, set[item], logOddsRatio);
            if (double.IsNaN(logOddsRatio))
                tuple.Color = Color.magenta;
            else if (double.IsPositiveInfinity(logOddsRatio))
                tuple.Color = Color.red;
            else if (double.IsNegativeInfinity(logOddsRatio))
                tuple.Color = Color.blue;
            else if (logOddsRatio > double.NegativeInfinity && logOddsRatio <= 0)
                tuple.Color = Color.Lerp(Color.blue, Color.green, (float)logOddsRatio);
            else if (logOddsRatio > 0 && logOddsRatio < double.PositiveInfinity)
                tuple.Color = Color.Lerp(Color.yellow, Color.red, (float)logOddsRatio);                
            results.Add(tuple);
        }
        ColorScale.callFunctionOnce = false;
    }

    public static void CalculateMatthewsCorrelationCoefficient(Dictionary<Vector4, Vector3> set)
    {
        results = new List<Tuple>();       
        Tuple tuple = new Tuple();

        foreach (var item in set.Keys)
        {            
            double totalPositiveActual = item.x + item.y;
            double totalPositivePredicted = item.x + item.z;
            double totalNegativeActual = item.z + item.w;
            double totalNegativePredicted = item.y + item.w;
            double matthewsCoefficient = ((item.x * item.w) - (item.y * item.z)) / Math.Sqrt(totalNegativePredicted * totalNegativeActual * totalPositiveActual * totalPositivePredicted);
            tuple = new Tuple(item, set[item], matthewsCoefficient);
            if (double.IsNaN(matthewsCoefficient))
                tuple.Color = Color.magenta;
            else if (double.IsPositiveInfinity(matthewsCoefficient))
                tuple.Color = Color.red;
            else if (matthewsCoefficient == 1)
                tuple.Color = Color.red;
            else if (matthewsCoefficient <= 0)
                tuple.Color = Color.Lerp(Color.blue, Color.green, (float)matthewsCoefficient);
            else                
                tuple.Color = Color.Lerp(GiveColor(matthewsCoefficient).GetOldColor, GiveColor(matthewsCoefficient).GetNewColor, GiveColor(matthewsCoefficient).GetValue);
            results.Add(tuple);
        }
        ColorScale.callFunctionOnce = false;
    }

    public static void CalculateNegativePredictiveValue(Dictionary<Vector4, Vector3> set)
    {
        results = new List<Tuple>();        
        Tuple tuple = new Tuple();

        foreach (var item in set.Keys)
        {            
            double negativePredictiveValue = item.w / (item.z + item.w);
            tuple = new Tuple(item, set[item], negativePredictiveValue);
            if (double.IsNaN(negativePredictiveValue))
                tuple.Color = Color.magenta;
            else if (double.IsPositiveInfinity(negativePredictiveValue))
                tuple.Color = Color.red;
            else
                tuple.Color = Color.Lerp(GiveColor(negativePredictiveValue).GetOldColor, GiveColor(negativePredictiveValue).GetNewColor, GiveColor(negativePredictiveValue).GetValue);
            results.Add(tuple);
        }
        ColorScale.callFunctionOnce = false;
    }

    public static void CalculateOptimizedPrecision(Dictionary<Vector4, Vector3> set)
    {
        results = new List<Tuple>();        
        Tuple tuple = new Tuple();

        foreach (var item in set.Keys)
        {            
            double accuracy = (item.x + item.w) / (item.x + item.y + item.z + item.w);
            double specificity = item.w / (item.w + item.y);
            double sensitivity = item.x / (item.x + item.z);
            double optimizedPrecision = accuracy - (Math.Abs(specificity - sensitivity) / (specificity + sensitivity));
            tuple = new Tuple(item, set[item], optimizedPrecision);
            if (double.IsNaN(optimizedPrecision))
                tuple.Color = Color.magenta;
            else if (double.IsPositiveInfinity(optimizedPrecision) || optimizedPrecision == 1)
                tuple.Color = Color.red;
            else if (optimizedPrecision < 0.3)
                tuple.Color = Color.Lerp(Color.blue, Color.green, (float)optimizedPrecision);
            else if (optimizedPrecision > 0.3 && optimizedPrecision <=0.5)
                tuple.Color = Color.Lerp(Color.green, Color.yellow, (float)optimizedPrecision);
            else
                tuple.Color = Color.Lerp(Color.yellow, Color.red, (float)optimizedPrecision);            
            results.Add(tuple);
        }
        ColorScale.callFunctionOnce = false;
    }

    public static void CalculatePointwiseAUCROC(Dictionary<Vector4, Vector3> set)
    {
        results = new List<Tuple>();        
        Tuple tuple = new Tuple();

        foreach (var item in set.Keys)
        {            
            double pointwise = (item.x / (item.x + item.z)) * (item.w / (item.y + item.w));
            tuple = new Tuple(item, set[item], pointwise);
            if (double.IsNaN(pointwise))
                tuple.Color = Color.magenta;
            else if (double.IsPositiveInfinity(pointwise))
                tuple.Color = Color.red;
            else
                tuple.Color = Color.Lerp(GiveColor(pointwise).GetOldColor, GiveColor(pointwise).GetNewColor, GiveColor(pointwise).GetValue);
            results.Add(tuple);
        }
        ColorScale.callFunctionOnce = false;
    }

    public static void CalculatePositivePredictiveValue(Dictionary<Vector4, Vector3> set)
    {
        results = new List<Tuple>();        
        Tuple tuple = new Tuple();

        foreach (var item in set.Keys)
        {            
            double positiveValue = item.x / (item.x + item.y);
            tuple = new Tuple(item, set[item], positiveValue);
            if (double.IsNaN(positiveValue))
                tuple.Color = Color.magenta;
            else if (double.IsPositiveInfinity(positiveValue))
                tuple.Color = Color.red;
            else
                tuple.Color = Color.Lerp(GiveColor(positiveValue).GetOldColor, GiveColor(positiveValue).GetNewColor, GiveColor(positiveValue).GetValue);
            results.Add(tuple);
        }
        ColorScale.callFunctionOnce = false;
    }

    public static void CalculatePrecision(Dictionary<Vector4, Vector3> set)
    {
        results = new List<Tuple>();        
        Tuple tuple = new Tuple();

        foreach (var item in set.Keys)
        {            
            double precision = item.x / (item.x + item.y);
            tuple = new Tuple(item, set[item], precision);
            if (double.IsNaN(precision))
                tuple.Color = Color.magenta;
            else if (double.IsPositiveInfinity(precision))
                tuple.Color = Color.red;
            else
                tuple.Color = Color.Lerp(GiveColor(precision).GetOldColor, GiveColor(precision).GetNewColor, GiveColor(precision).GetValue);
            results.Add(tuple);
        }
        ColorScale.callFunctionOnce = false;
    }

    public static void CalculateRecall(Dictionary<Vector4, Vector3> set)
    {
        results = new List<Tuple>();        
        Tuple tuple = new Tuple();

        foreach (var item in set.Keys)
        {            
            double recall = item.x / (item.x + item.z);
            tuple = new Tuple(item, set[item], recall);
            if (double.IsNaN(recall))
                tuple.Color = Color.magenta;
            else if (double.IsPositiveInfinity(recall))
                tuple.Color = Color.red;
            else
                tuple.Color = Color.Lerp(GiveColor(recall).GetOldColor, GiveColor(recall).GetNewColor, GiveColor(recall).GetValue);
            results.Add(tuple);
        }
        ColorScale.callFunctionOnce = false;
    }

    public static void CalculateSensitivity(Dictionary<Vector4, Vector3> set)
    {
        results = new List<Tuple>();        
        Tuple tuple = new Tuple();

        foreach (var item in set.Keys)
        {            
            double sensitivity = item.x / (item.x + item.z);
            tuple = new Tuple(item, set[item], sensitivity);
            if (double.IsNaN(sensitivity))
                tuple.Color = Color.magenta;
            else if (double.IsPositiveInfinity(sensitivity))
                tuple.Color = Color.red;
            else
                tuple.Color = Color.Lerp(GiveColor(sensitivity).GetOldColor, GiveColor(sensitivity).GetNewColor, GiveColor(sensitivity).GetValue);
            results.Add(tuple);
        }
        ColorScale.callFunctionOnce = false;
    }

    public static void CalculateSpecificity(Dictionary<Vector4, Vector3> set)
    {
        results = new List<Tuple>();        
        Tuple tuple = new Tuple();

        foreach (var item in set.Keys)
        {           
            double specificity = item.w / (item.w + item.y);
            tuple = new Tuple(item, set[item], specificity);
            if (double.IsNaN(specificity))
                tuple.Color = Color.magenta;
            else if (double.IsPositiveInfinity(specificity))
                tuple.Color = Color.red;
            else
                tuple.Color = Color.Lerp(GiveColor(specificity).GetOldColor, GiveColor(specificity).GetNewColor, GiveColor(specificity).GetValue);
            results.Add(tuple);
        }
        ColorScale.callFunctionOnce = false;
    }

    public static void CalculateTrueNegativeRate(Dictionary<Vector4, Vector3> set)
    {
        results = new List<Tuple>();        
        Tuple tuple = new Tuple();

        foreach (var item in set.Keys)
        {            
            double trueNegativeRate = item.w / (item.w + item.y);
            tuple = new Tuple(item, set[item], trueNegativeRate);
            if (double.IsNaN(trueNegativeRate))
                tuple.Color = Color.magenta;
            else if (double.IsPositiveInfinity(trueNegativeRate))
                tuple.Color = Color.red;
            else
                tuple.Color = Color.Lerp(GiveColor(trueNegativeRate).GetOldColor, GiveColor(trueNegativeRate).GetNewColor, GiveColor(trueNegativeRate).GetValue);
            results.Add(tuple);
        }
        ColorScale.callFunctionOnce = false;
    }

    public static void CalculateTruePositiveRate(Dictionary<Vector4, Vector3> set)
    {
        results = new List<Tuple>();        
        Tuple tuple = new Tuple();

        foreach (var item in set.Keys)
        {            
            double truePositiveRate = item.x / (item.x + item.z);
            tuple = new Tuple(item, set[item], truePositiveRate);
            if (double.IsNaN(truePositiveRate))
                tuple.Color = Color.magenta;
            else if (double.IsPositiveInfinity(truePositiveRate))
                tuple.Color = Color.red;
            else
                tuple.Color = Color.Lerp(GiveColor(truePositiveRate).GetOldColor, GiveColor(truePositiveRate).GetNewColor, GiveColor(truePositiveRate).GetValue);
            results.Add(tuple);
        }
        ColorScale.callFunctionOnce = false;
    }

    public static Row GiveColor(double result)
    {
        Row colors;
        float scale = (float)result * (float)(colorsArray.Length - 1);        
        Color oldColor;
        if (double.IsNegativeInfinity(result))  
            oldColor = colorsArray[0];
        else if (scale < 0)
            oldColor = colorsArray[0];
        else if (scale > 4)
            oldColor = colorsArray[3];
        else
            oldColor = colorsArray[(int)scale];
        Color newColor;
        if (double.IsNegativeInfinity(result))
            newColor = colorsArray[0];
        if (double.IsPositiveInfinity(result))
            newColor = colorsArray[3];
        else if (result >=1 )
            newColor = colorsArray[(int)(scale + 1f - 1)];
        else
            newColor = colorsArray[(int)(scale + 1f)];

        float newT = scale - Mathf.Round(scale);
             
        colors = new Row(oldColor, newColor, newT);

        return colors;
    }
}
