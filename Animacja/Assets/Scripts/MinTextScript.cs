﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MinTextScript : MonoBehaviour {

    TextMesh mesh;
	
	void Start () {
        mesh = GetComponent<TextMesh>();
	}
		
	void Update () {

		if (!ColorScale.callFunctionOnce)
        {
            List<Tuple> list = new List<Tuple>();

            foreach (var item in Equations.Results)
            {
                if (!double.IsNaN(item.Value))
                    list.Add(item);
            }

            var min = Math.Round(list.Min(x => x.Value), 2); 
            mesh.text = min.ToString();
        }
	}
}
