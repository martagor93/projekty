﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour {

    public Button button;

    public void LoadScene_Click()
    {
        Tetrahedron.Matrix3 = new Matrix3(HowManyPoints.VectorSum);
        if (HowManyPoints.VectorSum == 64)
            Tetrahedron.scale = true;
        SceneManager.LoadScene("scene1");
    }
}
