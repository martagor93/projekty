﻿using System.Linq;
using UnityEngine;

public class StatementScript : MonoBehaviour
{

    public Input input;
    private static string wholeText;

    public void Input_StatementChanged(string text)
    {
        wholeText = new string(text.OrderBy(c => c).ToArray());
    }

    public static string WholeText
    {
        get { return wholeText; }
        private set { wholeText = value; }
    }
}
