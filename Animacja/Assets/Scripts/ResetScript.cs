﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResetScript : MonoBehaviour {

    public Button button; 
	public void Button_Click()
    {        
        Tetrahedron.ResetScene();
    }

    public void StopAnimation_Click()
    {
        Tetrahedron.stopAnimation = true;
    }

    public void ResumeAnimation_Click()
    {
        Tetrahedron.stopAnimation = false;
    }
}
