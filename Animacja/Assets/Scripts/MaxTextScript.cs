﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MaxTextScript : MonoBehaviour {

    TextMesh mesh;

    void Start()
    {
        mesh = GetComponent<TextMesh>();
    }

    void Update()
    {

        if (!ColorScale.callFunctionOnce)
        {
            List<Tuple> list = new List<Tuple>();

            foreach (var item in Equations.Results)
            {
                if (!double.IsNaN(item.Value))
                    list.Add(item);
            }
                                 
            var max = Math.Round(list.Max(x => x.Value), 2);
            mesh.text = max.ToString();
        }
    }
}
