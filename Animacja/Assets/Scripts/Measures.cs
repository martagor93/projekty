﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class Measures : MonoBehaviour
{

    public Dropdown list;    

    private void Start()
    {
        List<string> measureNames = new List<string>()
        {
            "Proszę wybrać miarę",
            "Accuracy",
            "Area Under Lift",
            "Balanced Accuracy",
            "Diagnostic odds ratio",
            "F1 score",
            "False negative rate",
            "False positive rate",
            "G-mean",
            "Jaccard coefficient",
            "Kappa",
            "Log odds-ratio",
            "Matthews correlation coefficient",
            "Negative predictive value",
            "Optimized precision",
            "Pointwise AUC-ROC",
            "Positive predictive value",
            "Precision",
            "Recall",
            "Sensitivity",
            "Specificity",
            "True negative rate",
            "True positive rate"
        };
        list.AddOptions(measureNames);
    }

    private void Update()
    {
        if (Equations.Results == null)
            return;
    }

    public void Dropdown_IndexChanged(int index)
    {
        switch (index)
        {
            case 0:
                Tetrahedron.DrawGizmosBasedOnMeasures(false);
                break;
            case 1:
                Equations.CalculateAccuracy(Tetrahedron.Matrix3.Pairs);
                Tetrahedron.DrawGizmosBasedOnMeasures(true);
                break;
            case 2:
                Equations.CalculateAreaUnderLift(Tetrahedron.Matrix3.Pairs);
                Tetrahedron.DrawGizmosBasedOnMeasures(true);
                break;
            case 3:
                Equations.CalculateBalancedAccuracy(Tetrahedron.Matrix3.Pairs);
                Tetrahedron.DrawGizmosBasedOnMeasures(true);
                break;
            case 4:
                Equations.CalculateDiagnosticOddsRatio(Tetrahedron.Matrix3.Pairs);
                Tetrahedron.DrawGizmosBasedOnMeasures(true);
                break;
            case 5:
                Equations.CalculateF1Score(Tetrahedron.Matrix3.Pairs);
                Tetrahedron.DrawGizmosBasedOnMeasures(true);
                break;
            case 6:
                Equations.CalculateFalseNegativeRate(Tetrahedron.Matrix3.Pairs);
                Tetrahedron.DrawGizmosBasedOnMeasures(true);
                break;
            case 7:
                Equations.CalculateFalsePositiveRate(Tetrahedron.Matrix3.Pairs);
                Tetrahedron.DrawGizmosBasedOnMeasures(true);
                break;
            case 8:
                Equations.CalculateGMean(Tetrahedron.Matrix3.Pairs);
                Tetrahedron.DrawGizmosBasedOnMeasures(true);
                break;
            case 9:
                Equations.CalculateJaccardCoefficient(Tetrahedron.Matrix3.Pairs);
                Tetrahedron.DrawGizmosBasedOnMeasures(true);
                break;
            case 10:
                Equations.CalculateKappa(Tetrahedron.Matrix3.Pairs);
                Tetrahedron.DrawGizmosBasedOnMeasures(true);
                break;
            case 11:
                Equations.CalculateLogOddsRatio(Tetrahedron.Matrix3.Pairs);
                Tetrahedron.DrawGizmosBasedOnMeasures(true);
                break;
            case 12:
                Equations.CalculateMatthewsCorrelationCoefficient(Tetrahedron.Matrix3.Pairs);
                Tetrahedron.DrawGizmosBasedOnMeasures(true);
                break;
            case 13:
                Equations.CalculateNegativePredictiveValue(Tetrahedron.Matrix3.Pairs);
                Tetrahedron.DrawGizmosBasedOnMeasures(true);
                break;
            case 14:
                Equations.CalculateOptimizedPrecision(Tetrahedron.Matrix3.Pairs);
                Tetrahedron.DrawGizmosBasedOnMeasures(true);
                break;
            case 15:
                Equations.CalculatePointwiseAUCROC(Tetrahedron.Matrix3.Pairs);
                Tetrahedron.DrawGizmosBasedOnMeasures(true);
                break;
            case 16:
                Equations.CalculatePositivePredictiveValue(Tetrahedron.Matrix3.Pairs);
                Tetrahedron.DrawGizmosBasedOnMeasures(true);
                break;
            case 17:
                Equations.CalculatePrecision(Tetrahedron.Matrix3.Pairs);
                Tetrahedron.DrawGizmosBasedOnMeasures(true);
                break;
            case 18:
                Equations.CalculateRecall(Tetrahedron.Matrix3.Pairs);
                Tetrahedron.DrawGizmosBasedOnMeasures(true);
                break;
            case 19:
                Equations.CalculateSensitivity(Tetrahedron.Matrix3.Pairs);
                Tetrahedron.DrawGizmosBasedOnMeasures(true);
                break;
            case 20:
                Equations.CalculateSpecificity(Tetrahedron.Matrix3.Pairs);
                Tetrahedron.DrawGizmosBasedOnMeasures(true);
                break;
            case 21:
                Equations.CalculateTrueNegativeRate(Tetrahedron.Matrix3.Pairs);
                Tetrahedron.DrawGizmosBasedOnMeasures(true);
                break;
            case 22:
                Equations.CalculateTruePositiveRate(Tetrahedron.Matrix3.Pairs);
                Tetrahedron.DrawGizmosBasedOnMeasures(true);
                break;
            default:
                break;
        }
    }    
}
