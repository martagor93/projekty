﻿using System;
using UnityEngine;
using System.Linq;
using System.Collections.Generic;

public class Intersection : MonoBehaviour
{

    public Input input;
    public static int value;

    public void Input_TextChanged(string text)
    {
        try
        {
            value = Int32.Parse(text);
            RemovePointsToCreateIntersection();
        }
        catch (Exception exception)
        {
            //MessageBox.Show("Proszę wprowadzić liczbę");
        }
    }

    public void RemovePointsToCreateIntersection()
    {
        List<Tuple> list = new List<Tuple>();

        switch (StatementScript.WholeText)
        {
            case "a":
                list = Equations.Results.FindAll(x => x.Vector.x <= value);
                break;
            case "b":
                list = Equations.Results.FindAll(x => x.Vector.y <= value);
                break;
            case "c":
                list = Equations.Results.FindAll(x => x.Vector.z <= value);
                break;
            case "d":
                list = Equations.Results.FindAll(x => x.Vector.w <= value);
                break;
            case "ab":
                list = Equations.Results.FindAll(x => (x.Vector.x + x.Vector.y) <= value);
                break;
            case "ac":
                list = Equations.Results.FindAll(x => (x.Vector.x + x.Vector.z) <= value);
                break;
            case "ad":
                list = Equations.Results.FindAll(x => (x.Vector.x + x.Vector.w) <= value);
                break;
            case "bc":
                list = Equations.Results.FindAll(x => (x.Vector.z + x.Vector.z) <= value);
                break;
            case "bd":
                list = Equations.Results.FindAll(x => (x.Vector.y + x.Vector.w) <= value);
                break;
            case "cd":
                list = Equations.Results.FindAll(x => (x.Vector.z + x.Vector.w) <= value);
                break;
            case "abc":
                list = Equations.Results.FindAll(x => (x.Vector.x + x.Vector.y + x.Vector.z) <= value);
                break;
            case "abd":
                list = Equations.Results.FindAll(x => (x.Vector.x + x.Vector.y + x.Vector.w) <= value);
                break;
            case "acd":
                list = Equations.Results.FindAll(x => (x.Vector.z + x.Vector.x + x.Vector.w) <= value);
                break;
            case "abcd":
                list = Equations.Results.FindAll(x => (x.Vector.z + x.Vector.y + x.Vector.w + x.Vector.x) <= value);
                break;                
        }

        for (int i = 0; i < list.Count; i++)
        {
            GameObject sphere = Tetrahedron.spheres.Find(x => x.transform.localPosition == list[i].Vector3);

            if (sphere != null)
                sphere.SetActive(false);
        }
    }
}
