﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ColorScale : MonoBehaviour {

    public static bool callFunctionOnce = true;

    private void Update()
    {
        if (!callFunctionOnce)
        {
            callFunctionOnce = true;
            AssignColors();
        }
    }

    private void AssignColors()
    {
        List<Tuple> list = new List<Tuple>();
        
        foreach(var item in Equations.Results)
        {
            if (!double.IsNaN(item.Value))
                list.Add(item);
        }        
                
        list = list.GroupBy(x => x.Color).Select(y => y.First()).ToList();
        list = list.OrderBy(x => x.Value).ToList();

        var texture = new Texture2D(list.Count, list.Count, TextureFormat.ARGB32, false);
        GetComponent<Renderer>().material.mainTexture = texture;

        for (int i=0; i<texture.height; i++)
        {
            for (int j=0; j<texture.width; j++)
            {
                texture.SetPixel(j, i, list[j].Color);
            }
        }

        texture.Apply();
    }
}
