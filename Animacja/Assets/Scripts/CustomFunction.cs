﻿using System.Collections.Generic;
using UnityEngine;
using NCalc;

public class CustomFunction : MonoBehaviour {

    public Input input;

    public void Input_TextChanged(string text)
    {
        Expression expression = new Expression(text);        
        Tuple tuple = new Tuple();
        List<Tuple> results = new List<Tuple>();
        string oldText = text;
        Vector4 vector = new Vector4();

        foreach(var item in Tetrahedron.Matrix3.Pairs.Keys)
        {
            Expression e;
            vector = item;

            if (text.Contains("a"))
            {
                text = text.Replace("a", vector.x.ToString());
            }
            if (text.Contains("b"))
            {
                text = text.Replace("b", vector.y.ToString());
            }
            if (text.Contains("c"))
            {
                text = text.Replace("c", vector.z.ToString());
            }
            if (text.Contains("d"))
            {
                text = text.Replace("d", vector.w.ToString());
            }

            e = new Expression(text);
            double result = double.Parse(e.Evaluate().ToString());

            tuple = new Tuple(item, Tetrahedron.Matrix3.Pairs[item], result);
            if (double.IsNaN(result))
                tuple.Color = Color.magenta;
            else if (double.IsPositiveInfinity(result))
                tuple.Color = Color.red;
            else
                tuple.Color = Color.Lerp(Color.yellow, Color.red, (float)result/24);

            results.Add(tuple);
            text = oldText;
        }
        
        Equations.Results = results;
        Tetrahedron.DrawGizmosBasedOnMeasures(true);
        ColorScale.callFunctionOnce = false;
    }
}
